
#include "basic_requirement.h"



int main(void)
{
    int eStatus;

    gui_dispParameter =1;


	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer


	// Configure all GPIO to Output Low
	    P1OUT = 0x00;P2OUT = 0x00;P3OUT = 0x00;P4OUT = 0x00;
	    P5OUT = 0x00;P6OUT = 0x00;P7OUT = 0x00;P8OUT = 0x00;

	    P1DIR = 0xFF;P2DIR = 0xFF;P3DIR = 0xFF;P4DIR = 0xFF;
	    P5DIR = 0xFF;P6DIR = 0xFF;P7DIR = 0xFF;P8DIR = 0xFF;




	//	gul_VolperPulse = FRAM_guiVolperPulse;
//    FRAM_guiVolperPulse = 123456;
    FRAM_guiVolperPulse = 10000000;
    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
    _nop();
    gf_VolperPulse = gf_VolperPulse/1000;
	_nop();


//	gd_CT_LTR = FRAM_gud_CT_LTR;
//	gdl_TT_LTR = FRAM_gud_TT_LTR;


	init_oscilator();
	init_pulse_port();
	Init_Keys();
	init_SegmentLCD();
	init_Timer0_A1();
	Timer0_A1_ON();
	init_Timer1_A3();

	//          Disable the GPIO power-on default high-impedance mode
	//          to activate previously configured port settings
	           PM5CTL0 &= ~LOCKLPM5;

	display_Batch();
	guiKeyDebounce=0;
	gul_5ms_Count=0;
	guiScreenDisplayTimer=1000;
	 __bis_SR_register(GIE);     // Enter LPM3

//    PMMCTL0_H = PMMPW_H;                // Open PMM Registers for write
//    PMMCTL0_L |= PMMREGOFF;             // and set PMMREGOFF
//    PMMCTL0_H = 0;                      // Lock PMM Registers
//	 __bis_SR_register(LPM4_bits|GIE); // Enter LPM3 w/interrupt

	 __no_operation();



	while(1)
	{
//	    __bis_SR_register(LPM4_bits|GIE); // Enter LPM3 w/interrupt


	    if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
	    {
	        eStatus= detectKey();
	        guiKeyDebounce=0;
	    }


	    if((eStatus == SCROLL_KEY)  && (guc_SetupMode==0))
	    {
	        eStatus=0;
	        gui_dispParameter++;
	        guiScreenDisplayTimer=1000;
	        if(gui_dispParameter>3)
	            gui_dispParameter =1;
	    }

        setup_Mode();

	    if(guiScreenDisplayTimer>1000)
	    {
	        guiScreenDisplayTimer=0;
            if((gui_dispParameter==1) && (guc_SetupMode==0))
            {
                display_Batch();
            }
            else if((gui_dispParameter==2) && (guc_SetupMode==0))
            {
                display_Total((unsigned long int)FRAM_gud_TT_LTR);
            }
            else if((gui_dispParameter==3) && (guc_SetupMode==0))
            {
                display_Flowrate(gul_FlowRateLPH);
            }
	    }
	}
}


