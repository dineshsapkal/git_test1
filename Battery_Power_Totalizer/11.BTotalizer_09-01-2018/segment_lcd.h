/*
 * segment_lcd.h
 *
 *  Created on: 20-Dec-2017
 *      Author: Dell
 */

#ifndef SEGMENT_LCD_H_
#define SEGMENT_LCD_H_


extern unsigned char gui_dispParameter;


void init_SegmentLCD();

extern void display_digit(unsigned char,unsigned char);
extern void display_Value(unsigned long);
extern void displayScale(unsigned long);


extern void clear_data();
extern void display_Batch();
extern void display_Total(unsigned long int);
extern void display_Flowrate(unsigned long int);
extern void display_ScaleFator(unsigned long int);


#endif /* SEGMENT_LCD_H_ */
