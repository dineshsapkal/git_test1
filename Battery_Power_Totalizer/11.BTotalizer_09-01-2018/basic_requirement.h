/*
 * basic_requirement.h
 *
 *  Created on: 19-Dec-2017
 *      Author: Dell
 */

#ifndef BASIC_REQUIREMENT_H_
#define BASIC_REQUIREMENT_H_




#define XTAL_Freq   8000000.00  //  selected  frequency
#define OneCycleTime ((1.00/XTAL_Freq)*1000000)     //conveting

//#define LCD4Bit
//#define DEBUG

#define TRUE    1
#define FALSE   0



#include <msp430.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>


#include "pulse.h"
#include "keys.h"
#include "segment_lcd.h"
#include "timer.h"
#include "setup.h"

extern unsigned long lenBat;
extern void init_oscilator();



#endif /* BASIC_REQUIREMENT_H_ */
