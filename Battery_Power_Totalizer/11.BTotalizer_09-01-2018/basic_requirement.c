/*
 * basic_requirement.c
 *
 *  Created on: 19-Dec-2017
 *      Author: Dell
 */


#include "basic_requirement.h"





void init_oscilator()
{
    // Configure XT1 oscillator
       P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins 323768KHz External Crystal
       do
       {
           CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
           SFRIFG1 &= ~OFIFG;
       }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
//       CSCTL6 = (CSCTL6 & ~(XT1DRIVE_3)) | XT1DRIVE_2;            // Higher drive strength and current consumption for XT1 oscillator


//       // Disable the GPIO power-on default high-impedance mode
//       // to activate previously configured port settings
//       PM5CTL0 &= ~LOCKLPM5;
}
