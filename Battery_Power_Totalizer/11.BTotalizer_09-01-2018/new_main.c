
#include "basic_requirement.h"
void initGpio(void);

int main(void)
{
    int eStatus;

    gui_dispParameter =1;
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    //  gul_VolperPulse = FRAM_guiVolperPulse;
    FRAM_guiVolperPulse = 123456;
    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
    _nop();
    gf_VolperPulse = gf_VolperPulse/1000;
    _nop();

    // Configure GPIO
    initGpio();

    // Determine whether we are coming out of an LPMx.5 or a regular RESET.
    if (SYSRSTIV == SYSRSTIV_LPM5WU)        // MSP430 just woke up from LPMx.5
    {

        __no_operation();
        // Initialize XT1 32kHz crystal
        P4SEL0 |= BIT1 | BIT2;              // set XT1 pin as second function
        do
        {
            CSCTL7 &= ~(XT1OFFG | DCOFFG);  // Clear XT1 and DCO fault flag
            SFRIFG1 &= ~OFIFG;
        } while (SFRIFG1 & OFIFG);          // Test oscillator fault flag

        if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
        {
            eStatus= detectKey();
            guiKeyDebounce=0;
        }


        if((eStatus == SCROLL_KEY)  && (guc_SetupMode==0))
        {
            eStatus=0;
            gui_dispParameter++;
            guiScreenDisplayTimer=1000;
            if(gui_dispParameter>3)
                gui_dispParameter =1;
        }

        setup_Mode();

        if(guiScreenDisplayTimer>1000)
        {
            guiScreenDisplayTimer=0;
            if((gui_dispParameter==1) && (guc_SetupMode==0))
            {
                display_Batch();
            }
            else if((gui_dispParameter==2) && (guc_SetupMode==0))
            {
                display_Total((unsigned long int)FRAM_gud_TT_LTR);
            }
            else if((gui_dispParameter==3) && (guc_SetupMode==0))
            {
                display_Flowrate(gul_FlowRateLPH);
            }
        }

        __no_operation();
    }
    else
    {
        // Device powered up from a cold start.
        // It configures the device and puts the device into LPM4.5

        init_pulse_port();
        Init_Keys();
        init_SegmentLCD();
        init_Timer0_A1();
        Timer0_A1_ON();
        init_Timer1_A3();


        display_Batch();
        guiKeyDebounce=0;
        gul_5ms_Count=0;
        guiScreenDisplayTimer=1000;


        PMMCTL0_H = PMMPW_H;                // Open PMM Registers for write
        PMMCTL0_L &= ~(SVSHE);              // Disable high-side SVS
        PMMCTL0_L |= PMMREGOFF;             // and set PMMREGOFF
        PMMCTL0_H = 0;                      // Lock PMM Registers

        // Enter LPM4 Note that this operation does not return. The LPM4.5
        // will exit through a RESET event, resulting in a re-start
        // of the code.
        __bis_SR_register(LPM4_bits | GIE);
        __no_operation();
    }
}

void initGpio()
{
    P1DIR = 0xFB; P2DIR = 0xFF; P3DIR = 0xFF; P4DIR = 0xFF;
    P5DIR = 0xFF; P6DIR = 0xFF; P7DIR = 0xFF; P8DIR = 0xFF;
//    P1REN = 0xFF; P2REN = 0xFF; P3REN = 0xFF; P4REN = 0xFF;
//    P5REN = 0xFF; P6REN = 0xFF; P7REN = 0xFF; P8REN = 0xFF;
    P1OUT = 0x00; P2OUT = 0x00; P3OUT = 0x00; P4OUT = 0x00;
    P5OUT = 0x00; P6OUT = 0x00; P7OUT = 0x00; P8OUT = 0x00;

    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}
