/*
 * pulse.c
 *
 *  Created on: 03-Oct-2017
 *      Author: Dell
 */


#include "basic_requirement.h"


#pragma PERSISTENT(FRAM_gud_CT_LTR)
double FRAM_gud_CT_LTR = 0;

#pragma PERSISTENT(FRAM_gud_TT_LTR)
double FRAM_gud_TT_LTR = 0;



unsigned int gui_PulseContForFlowRate=0;

//double gd_CT_LTR=0,gdl_TT_LTR=0;



void init_pulse_port()
{
    PULSE_IN_PORTOUT &= ~PULSE_IN_PIN;              // Clear Pulse Pin output latch for a defined power-on state
//    PULSE_IN_PORT_SEL0 &= ~PULSE_IN_PIN;            // Set as GPIO

    PULSE_IN_PORT_DIR  &= ~ PULSE_IN_PIN;              // Configure Port Pin as pulled-up
    PULSE_IN_PORTOUT|= PULSE_IN_PIN;                   // select pull-down mode, 0
//    PULSE_IN_PIN_PULLUP |= PULSE_IN_PIN;              // enable internal pull up, 1

    PULSE_IN_PIN_IES |= PULSE_IN_PIN;
    PULSE_IN_PIN_INTERRUPT|= PULSE_IN_PIN;

    P1IFG=0;
}

// Port 1 interrupt service routine

void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
{

//    _nop();
//    gd_CT_LTR = gd_CT_LTR + gf_VolperPulse;
//    gdl_TT_LTR = gdl_TT_LTR + gf_VolperPulse;
//
//    SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
//    FRAM_gud_CT_LTR = gd_CT_LTR;
//    FRAM_gud_TT_LTR = gdl_TT_LTR;
//    SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
//
//
//    gui_PulseContForFlowRate++;
//
//    P1IFG &= ~PULSE_IN_PIN;                           // Clear P1.1 IFG
//    _nop();
//    P1IFG=0;


    _nop();
    SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
    FRAM_gud_CT_LTR = FRAM_gud_CT_LTR + gf_VolperPulse;
    FRAM_gud_TT_LTR = FRAM_gud_TT_LTR + gf_VolperPulse;
    SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)


    gui_PulseContForFlowRate++;

    P1IFG &= ~PULSE_IN_PIN;                           // Clear P1.1 IFG
    __bic_SR_register_on_exit(LPM4_bits);   // Exit LPM3
}
