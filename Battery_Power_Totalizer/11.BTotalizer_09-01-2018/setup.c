/*
 * setup.c
 *
 *  Created on: 25-Dec-2017
 *      Author: Dell
 */


#include "basic_requirement.h"

#pragma PERSISTENT(FRAM_guiVolperPulse)
unsigned long FRAM_guiVolperPulse = 0;


unsigned char guc_SetupMode;

float gf_VolperPulse;
unsigned long gul_VolperPulse;


void setup_Mode()
{
    unsigned char dtKey=0,digit8=0,digit7=0,digit6=0,digit5=0,digit4=0,digit3=0,digit2=0,digit1=0;
    char breakFlag=0;
    if(guc_SetupMode)
    {
        digit8 = (FRAM_guiVolperPulse%10);
        digit7 = (FRAM_guiVolperPulse/10)%10;
        digit6 = (FRAM_guiVolperPulse/100)%10;
        digit5 = (FRAM_guiVolperPulse/1000)%10;
        digit4 = (FRAM_guiVolperPulse/10000)%10;
        digit3 = (FRAM_guiVolperPulse/100000)%10;
        digit2 = (FRAM_guiVolperPulse/1000000)%10;
        digit1 = (FRAM_guiVolperPulse/10000000);

        _nop();
        //Digit8
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit8++;
                if(digit8>=10)
                {
                    digit8 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();

            display_digit(10,8);
            _delay_cycles(100000);

            display_digit(digit8,8);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit8,8);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit7
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit7++;
                if(digit7>=10)
                {
                    digit7 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();

            display_digit(10,7);
            _delay_cycles(100000);

            display_digit(digit7,7);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit7,7);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit6
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit6++;
                if(digit6>=10)
                {
                    digit6 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();

            display_digit(10,6);
            _delay_cycles(100000);

            display_digit(digit6,6);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit6,6);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit5
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit5++;
                if(digit5>=10)
                {
                    digit5 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();
            display_digit(10,5);
            _delay_cycles(100000);

            display_digit(digit5,5);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit5,5);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit4
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit4++;
                if(digit4>=10)
                {
                    digit4 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();
            display_digit(10,4);
            _delay_cycles(100000);

            display_digit(digit4,4);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit4,4);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit3
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit3++;
                if(digit3>=10)
                {
                    digit3 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();
            display_digit(10,3);
            _delay_cycles(100000);

            display_digit(digit3,3);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit3,3);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit2
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {
                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit2++;
                if(digit2>=10)
                {
                    digit2 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();

            display_digit(10,2);
            _delay_cycles(100000);

            display_digit(digit2,2);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit2,2);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
        //Digit1
        while(1)
        {
            if(guiKeyDebounce>100)                          //detect key after key KeyDebounce  800ms
            {

                dtKey= detectKey();
                guiKeyDebounce=0;
            }


            if(dtKey == SCROLL_KEY)
            {
                dtKey=0;
                digit1++;
                if(digit1>=10)
                {
                    digit1 = 0;
                }
            }
            else if(dtKey == SAVE_DIGIT)
            {
                dtKey = 0;
                breakFlag=1;
            }

            _nop();
            display_digit(10,1);
            _delay_cycles(100000);

            display_digit(digit1,1);
            _delay_cycles(100000);

            if(breakFlag)
            {
                display_digit(digit1,1);
                breakFlag=0;
                while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                guiKeyDebounce=0;
                break;
            }
        }
        _nop();
//
//        gul_VolperPulse = ((unsigned long)digit1 * 10000000) + ((unsigned long)digit2 * 1000000) + ((unsigned long)digit3 * 100000) + ((unsigned long)digit4 * 10000) + (digit5 * 1000) + (digit6 * 100) + (digit7 * 10) + (digit8 * 1);
//
//        //save into FRAM
//        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
//        FRAM_guiVolperPulse = gul_VolperPulse;
//        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
//
//        gf_VolperPulse = (float)gul_VolperPulse/10000;
//        gf_VolperPulse = gf_VolperPulse/1000;
//        _nop();
//


        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
        FRAM_guiVolperPulse = ((unsigned long)digit1 * 10000000) + ((unsigned long)digit2 * 1000000) + ((unsigned long)digit3 * 100000) + ((unsigned long)digit4 * 10000) + (digit5 * 1000) + (digit6 * 100) + (digit7 * 10) + (digit8 * 1);
        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)

        gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
        gf_VolperPulse = gf_VolperPulse/1000;
        _nop();



        _no_operation();
        guc_SetupMode = 0;
    }
}

