

#ifndef PULSE_H_
#define PULSE_H_




// defining the EXT_RESET key
#define PULSE_IN_PORTOUT           P1OUT
#define PULSE_IN_PORT_SEL0         P1SEL0
#define PULSE_IN_PORT_DIR          P1DIR
#define PULSE_IN_PIN               BIT7
#define PULSE_IN_PIN_PULLUP         P1REN

#define PULSE_IN_PIN_IES           P1IES
#define PULSE_IN_PIN_INTERRUPT     P1IE


//extern double gd_CT_LTR,gdl_TT_LTR;

extern double FRAM_gud_CT_LTR;
extern double FRAM_gud_TT_LTR;


extern unsigned int gui_PulseContForFlowRate;


extern void init_pulse_port();


#endif /* PULSE_H_ */
