/*
 * segment_lcd.c
 *
 *  Created on: 20-Dec-2017
 *      Author: Dell
 */




#include "basic_requirement.h"


unsigned char gui_dispParameter;

unsigned char d1,d2,d3,d4,d5,d6,d7,d8;

void init_SegmentLCD()
{
    // Configure LCD pins
//    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFC7;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
//    LCDVCTL = LCDCPEN | LCDREFEN | LCDSELVDD |VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM2,

    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected

    PMMCTL0_H = PMMPW_H;                                       // Open PMM Registers for write
    PMMCTL0_L |= PMMREGOFF_L;                                  // and set PMMREGOFF
    _nop();
}



void display_digit(unsigned char dispNum,unsigned char pos)
{
   if(pos==8)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[3] = 0x53;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 1:
                   LCDMEM[3] = 0x03;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 2:
                   LCDMEM[3] = 0x71;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x02;
                   break;
           case 3:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 4:
                   LCDMEM[3] = 0x23;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 5:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 6:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 7:
                   LCDMEM[3] = 0x13;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 8:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 9:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 10:
                  LCDMEM[3] = 0x00;
                  LCDMEM[4] = LCDMEM[4] & 0x30;
                  LCDMEM[4] = LCDMEM[4] | 0x00;
                  break;
       }
   }
   else if(pos==7)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x35;
                   break;
           case 1:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x00;
                   break;
           case 2:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x10;
                   LCDMEM[5] = 0x27;
                   break;
           case 3:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x07;
                   break;
           case 4:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x12;
                   break;
           case 5:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x17;
                   break;
           case 6:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x37;
                   break;
           case 7:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x01;
                   break;
           case 8:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x37;
                   break;
           case 9:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x17;
                   break;
           case 10:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   LCDMEM[5] = 0x00;
                   break;
       }
   }
   else  if(pos==6)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[6] = 0x53;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 1:
                   LCDMEM[6] = 0x03;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 2:
                   LCDMEM[6] = 0x71;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x02;
                   break;
           case 3:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 4:
                   LCDMEM[6] = 0x23;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 5:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 6:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 7:
                   LCDMEM[6] = 0x13;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 8:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 9:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 10:
                   LCDMEM[6] = 0x00;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
       }
   }
   else  if(pos==5)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x35;
                   break;
           case 1:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x00;
                   break;
           case 2:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x10;
                   LCDMEM[8] = LCDMEM[8] | 0x27;
                   break;
           case 3:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x07;
                   break;
           case 4:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x12;
                   break;
           case 5:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
           case 6:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 7:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x01;
                   break;
           case 8:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 9:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
           case 10:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   LCDMEM[8] = 0x00;
                   break;
       }
   }
   else  if(pos==4)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[9] = 0x53;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 1:
                   LCDMEM[9] = 0x03;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 2:
                   LCDMEM[9] = 0x71;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x02;
                   break;
           case 3:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 4:
                   LCDMEM[9] = 0x23;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 5:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 6:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 7:
                   LCDMEM[9] = 0x13;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 8:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 9:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 10:
                   LCDMEM[9] = 0x00;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
       }
   }
   else  if(pos==3)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x35;
                   break;
           case 1:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x00;
                   break;
           case 2:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x10;
                   LCDMEM[11] = LCDMEM[11] | 0x27;
                   break;
           case 3:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x07;
                   break;
           case 4:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x12;
                   break;
           case 5:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
           case 6:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 7:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x01;
                   break;
           case 8:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 9:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
           case 10:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   LCDMEM[11] = 0x00;
                   break;
       }
   }
   else  if(pos==2)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[12] = 0x53;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 1:
                   LCDMEM[12] = 0x03;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 2:
                   LCDMEM[12] = 0x71;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x02;
                   break;
           case 3:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 4:
                   LCDMEM[12] = 0x23;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 5:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 6:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 7:
                   LCDMEM[12] = 0x13;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 8:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 9:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 10:
                   LCDMEM[12] = 0x00;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
       }
   }
   else  if(pos==1)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x35;
                   break;
           case 1:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x00;
                   break;
           case 2:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x10;
                   LCDMEM[14] = LCDMEM[14] | 0x27;
                   break;
           case 3:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x07;
                   break;
           case 4:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x12;
                   break;
           case 5:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
           case 6:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 7:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x01;
                   break;
           case 8:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 9:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
           case 10:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   LCDMEM[14] = 0x00;
                   break;
       }
   }

}





void display_Value(unsigned long Value)
{
    if(Value<=9)
    {
        display_digit(d8,8);
    }
    else if(Value>9 && Value<=99)
    {
        display_digit(d8,8);
        display_digit(d7,7);
    }
    else if(Value>99 && Value<=999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if(Value>999 && Value<=9999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
    }
    else if(Value>9999 && Value<=99999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if(Value>99999 && Value<=999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if(Value>999999 && Value<=9999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if(Value>9999999 && Value<=99999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}

void clear_data()
{
    LCDMEM[3] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[5] = 0x00;
    LCDMEM[6] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[9] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[11] = 0x00;
    LCDMEM[12] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[14] = 0x00;
}
void display_Batch()
{
    d8 =  (long)((FRAM_gud_CT_LTR*10))%10;

    d7 = ((long)FRAM_gud_CT_LTR%10);
    d6 = ((long)FRAM_gud_CT_LTR/10)%10;
    d5 = ((long)FRAM_gud_CT_LTR/100)%10;
    d4 = ((long)FRAM_gud_CT_LTR/1000)%10;
    d3 = ((long)FRAM_gud_CT_LTR/10000)%10;
    d2 = ((long)FRAM_gud_CT_LTR/100000)%10;
    d1 = ((long)FRAM_gud_CT_LTR/1000000);


    clear_data();

    //for display Batch LTR
    LCDMEM[14] = 0x40;
    LCDMEM[8] = 0x40;
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;

    if((long)FRAM_gud_CT_LTR<=9)
    {
       display_digit(d8,8);
       display_digit(d7,7);
    }
    else if((long)FRAM_gud_CT_LTR>9 && (long)FRAM_gud_CT_LTR<=99)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if((long)FRAM_gud_CT_LTR>99 && (long)FRAM_gud_CT_LTR<=999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);

    }
    else if((long)FRAM_gud_CT_LTR>999 && (long)FRAM_gud_CT_LTR<=9999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if((long)FRAM_gud_CT_LTR>9999 && (long)FRAM_gud_CT_LTR<=99999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if((long)FRAM_gud_CT_LTR>99999 && (long)FRAM_gud_CT_LTR<=999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if((long)FRAM_gud_CT_LTR>999999 && (long)FRAM_gud_CT_LTR<=9999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}
void display_Total(unsigned long int Total)
{
    d8 =  Total%10;
    d7 = (Total/10)%10;
    d6 = (Total/100)%10;
    d5 = (Total/1000)%10;
    d4 = (Total/10000)%10;
    d3 = (Total/100000)%10;
    d2 = (Total/1000000)%10;
    d1 = (Total/10000000);

    clear_data();
    //for display Total LTS
    LCDMEM[13] = 0x04;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Total);
}
void display_Flowrate(unsigned long int Flow)
{
    d8 =  Flow%10;
    d7 = (Flow/10)%10;
    d6 = (Flow/100)%10;
    d5 = (Flow/1000)%10;
    d4 = (Flow/10000)%10;
    d3 = (Flow/100000)%10;
    d2 = (Flow/1000000)%10;
    d1 = (Flow/10000000);

    clear_data();

    //for display Flow Rate
    LCDMEM[11] = 0x40;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Flow);
}

void display_ScaleFator(unsigned long int ScaleFactor)
{

    clear_data();

    //for display Flow Rate
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    displayScale(ScaleFactor);
}


void displayScale(unsigned long Value)
{
    display_digit(((Value%10)),8);
    display_digit(((Value/10)%10),7);
    display_digit(((Value/100)%10),6);
    display_digit(((Value/1000)%10),5);
    display_digit(((Value/10000)%10),4);
    display_digit(((Value/100000)%10),3);
    display_digit(((Value/1000000)%10),2);
    display_digit((Value/10000000),1);
}
