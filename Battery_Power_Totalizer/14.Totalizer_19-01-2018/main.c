

/*************************************************************************************************************************************
 *
 *  Project Name :- Battery Power Totalizer
 *
 *  Date:- 19-01-2017
 *  1) Adding Flow rate Calculation on RTC 1Sec Interrupt to making current consumption low.
 *
*************************************************************************************************************************************/
#include "basic_requirement.h"


#pragma PERSISTENT(FRAM_gud_CT_LTR)
double FRAM_gud_CT_LTR = 0;

#pragma PERSISTENT(FRAM_gud_TT_LTR)
double FRAM_gud_TT_LTR = 0;




unsigned char gui_dispParameter;
unsigned int gui_PulseContForFlowRate=0;
long int  setupConter=0;                  //variable counter used after pressing two key to enter in setup mode
unsigned char breakFlag=0,passmatch=0,gucRTCConter=0;

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer

    gui_dispParameter =1;
    guc_SetupMode=0;

    //  gul_VolperPulse = FRAM_guiVolperPulse;
//    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
//    gul_VolperPulse = 158306;

    SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
    FRAM_guiVolperPulse = 959278;
    SYSCFG0 |= PFWP;
    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
    _nop();
    gf_VolperPulse = gf_VolperPulse/1000;
    _nop();





    init_gpio();
    initiate_Oscillator();



////---------- Initialize Pulse Pin-----------------
    P1DIR &= ~(BIT7);                          // Configure P1.3 as pulled-up
    P1OUT |= BIT7;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT7;                          // P1.3 pull-up register enable
    P1IES = 0;
    P1IE = 0;
    P1IES |= BIT7;                          // P1.3 Hi/Low edge
    P1IE |= BIT7;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

////---------- Initialize button Pin-----------------
    P1DIR &= ~(BIT5);                          // Configure P1.3 as pulled-up
//    P1OUT |= BIT5;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT5;                          // P1.3 pull-up register enable
    P1IES &= ~BIT5;                          // P1.3 Hi/Low edge
    P1IE |= BIT5;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

    P5DIR &= ~(BIT2);                          // Configure P1.3 as pulled-up


////------------------- Initiate Segment LCD ------------------------------------------------------
    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFC7;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM2,

    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected
    display_Batch();


////------------------- Initiate Segment LCD ------------------------------------------------------


////------------------------- Initiate The RTC ----------------------------------------------------
    // Initialize RTC
    // Interrupt and reset happen every 1024/32768 * 32 = 1 sec.
    RTCMOD = 32-1;
    RTCCTL = RTCSS__XT1CLK | RTCSR | RTCPS__1024 | RTCIE;
////------------------------- Initiate The RTC ----------------------------------------------------

    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

//    PMMCTL0_H = PMMPW_H;                                       // Open PMM Registers for write
//    PMMCTL0_L |= PMMREGOFF_L;                                  // and set PMMREGOFF

//    init_Timer0_A1();


    P1IFG &= ~BIT7;                         // P1.3 IFG cleared
    P1IFG &= ~BIT5;                         // P1.3 IFG cleared

    __bis_SR_register(GIE); // Enter LPM3 w/interrupt

    while(1)
    {
        if(guc_SetupMode)
        {
            setup_Mode();
        }
        else if(gucReset_CT_Flag)
        {
            gucReset_CT_Flag = 0;
            SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
            FRAM_gud_CT_LTR = 0;
            SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
            display_Batch();
            wait();
        }
        else if(gucReset_TT_Flag)
        {
            gucReset_TT_Flag = 0;
            SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
            FRAM_gud_TT_LTR = 0;
            SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
            display_Total((unsigned long int)FRAM_gud_TT_LTR);
            wait();
        }

        if((gui_dispParameter==1) && (guc_SetupMode==0))
        {
            display_Batch();
        }
        else if((gui_dispParameter==2) && (guc_SetupMode==0))
        {
            display_Total((unsigned long int)FRAM_gud_TT_LTR);
        }
        else if((gui_dispParameter==3) && (guc_SetupMode==0))
        {
            display_Flowrate(gul_FlowRateLPH);
        }

        if((gul_FlowRateLPH==0) && (guc_SetupMode==0))
        {
            if((gui_dispParameter==3) && (guc_SetupMode==0))
            {
                display_Flowrate(gul_FlowRateLPH);
            }
//            TA0CTL=0;
//            P1OUT |=BIT6;
//            gucSleepFlag=1;
            __bis_SR_register(LPM4_bits | GIE); // Enter LPM3 w/interrupt
//            gucSleepFlag=0;
//            P1OUT &= ~BIT6;
//            init_Timer0_A1();
//            Timer0_A1_ON();
        }
        __no_operation();                   // For debugger
    }
}

// Port 1 interrupt service routine

void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
{
    _nop();

       __bic_SR_register_on_exit(LPM4_bits);   // Exit LPM3
//    if(P1IFG&BIT7)
    if(P1IFG == 0x80)
    {
        P1IFG &= ~BIT7;                         // Clear P1.3 IFG
        _nop();
        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
        FRAM_gud_CT_LTR = FRAM_gud_CT_LTR + gf_VolperPulse;
        FRAM_gud_TT_LTR = FRAM_gud_TT_LTR + gf_VolperPulse;
        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
        gui_PulseContForFlowRate++;
    }
//    else if(P1IFG&BIT5)
    else if(P1IFG == 0x20)
    {
        _nop();
        setupConter=0;

        if(gui_dispParameter==1)
        {
            while(P1IN & BIT5)
            {
                _nop();
//                if((gui_dispParameter==1) && (guc_SetupMode == 0))
                if(guc_SetupMode == 0)
                {
                    setupConter++;
                    if(setupConter>45000)
                    {
                        _nop();


                        if (P5IN & BIT2)
                        {
                            setupConter=0;
                            guc_SetupMode=1;
    //                        display_ScaleFator(gul_VolperPulse);
                            breakFlag=1;
                            guc_SetupMode=1;
                            _no_operation();
                            break;
                        }
                        else if(setupConter>90000)
                        {
                            setupConter=0;
                            gucReset_CT_Flag = 1;
                            breakFlag=1;
                            break;
                        }
                    }
                }
//                else if((gui_dispParameter==1) && (guc_SetupMode == 1))
                else if(guc_SetupMode == 1)
                {
                    setupConter++;
                    if(setupConter>22550)
                    {
                        setupConter=0;
                        breakFlag=1;
                        guc_saveDataFlag=1;
                        break;
                    }
                }
            }

        }
        else if(gui_dispParameter == 2)
        {
            while(P1IN & BIT5)
            {
//                else if((gui_dispParameter==2) && (guc_SetupMode == 0))
                if(guc_SetupMode == 0)
                {
                    setupConter++;
                    __no_operation();
                    if(setupConter>45000)
                    {
                        _nop();
                        setupConter=0;
                        gucReset_TT_Flag = 1;
                        breakFlag=1;
                        break;
                    }
                }
            }
        }
        if( (breakFlag==0) && (guc_SetupMode==0))
        {
            gui_dispParameter++;
            if(gui_dispParameter>=4)
            {
                gui_dispParameter =1;
            }
        }
        else
        {
            breakFlag=0;
            gucButtonPressFlag=1;
        }
        P1IFG &= ~BIT5;                         // Clear P1.3 IFG
    }
    P1IFG=0;
}

void __attribute__ ((interrupt(RTC_VECTOR))) RTC_ISR (void)
{
    switch(__even_in_range(RTCIV, RTCIV_RTCIF))
    {
        case RTCIV_NONE : break;            // No interrupt pending
        case RTCIV_RTCIF:                   // RTC Overflow

            _nop();

//            if(!gucSleepFlag)
            gucRTCConter++;

            if(gucRTCConter>=2)
            {
                gucRTCConter=0;
                if(gui_PulseContForFlowRate!=0)
                {
                    if(gucFirstTime == 1)
                    {
                       gd_FlowRate[guiIndex] = gui_PulseContForFlowRate * gf_VolperPulse;

                       gd_FlowRateAvg=0;
                       for(len=0;len<=guiIndex;len++)
                       {
                           gd_FlowRateAvg = gd_FlowRateAvg + gd_FlowRate[len];
                       }
                       gd_FlowRateAvg_1=0;
                       gd_FlowRateAvg_1 = ((float)gd_FlowRateAvg/guiIndex);

                       gul_FlowRateLPH = 0;
                       gul_FlowRateLPH = gd_FlowRateAvg_1 * 1800;
                       guiIndex++;
                       gui_PulseContForFlowRate = 0;

                       if(guiIndex>10)
                       {
                           gucFirstTime=0;
                           guiIndex = 0;
                           gui_PulseContForFlowRate = 0;
                       }
                    }
                    else
                    {
                       gd_FlowRate[guiIndex] = gui_PulseContForFlowRate * gf_VolperPulse;
                       guiIndex++;
                       gui_PulseContForFlowRate = 0;

                       __bic_SR_register_on_exit(LPM4_bits);   // Exit LPM3



                       gd_FlowRateAvg=0;
                       for(len=0;len<=9;len++)
                       {
                           gd_FlowRateAvg = gd_FlowRateAvg + gd_FlowRate[len];
                       }
                       gd_FlowRateAvg_1 = (gd_FlowRateAvg/10.00);
                       gul_FlowRateLPH = gd_FlowRateAvg_1 * 1800;
                       if(guiIndex>10)
                       {
                           guiIndex = 0;
                           gul_5ms_Count = 0;
                           gui_PulseContForFlowRate = 0;
                       }
                   }
                   _nop();
                }

            }
            _nop();

            break;
        default:          break;
    }
}

