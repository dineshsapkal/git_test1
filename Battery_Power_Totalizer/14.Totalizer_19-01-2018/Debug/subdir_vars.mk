################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430fr4133.cmd 

C_SRCS += \
../basic_requirement.c \
../main.c \
../segment_lcd.c \
../setup.c \
../timer.c 

C_DEPS += \
./basic_requirement.d \
./main.d \
./segment_lcd.d \
./setup.d \
./timer.d 

OBJS += \
./basic_requirement.obj \
./main.obj \
./segment_lcd.obj \
./setup.obj \
./timer.obj 

OBJS__QUOTED += \
"basic_requirement.obj" \
"main.obj" \
"segment_lcd.obj" \
"setup.obj" \
"timer.obj" 

C_DEPS__QUOTED += \
"basic_requirement.d" \
"main.d" \
"segment_lcd.d" \
"setup.d" \
"timer.d" 

C_SRCS__QUOTED += \
"../basic_requirement.c" \
"../main.c" \
"../segment_lcd.c" \
"../setup.c" \
"../timer.c" 


