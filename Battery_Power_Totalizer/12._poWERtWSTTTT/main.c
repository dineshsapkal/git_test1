

#include <msp430.h>
#include "timer.h"
#include "setup.h"


//#pragma PERSISTENT(FRAM_guiVolperPulse)
//unsigned long FRAM_guiVolperPulse = 0;

#pragma PERSISTENT(FRAM_gud_CT_LTR)
double FRAM_gud_CT_LTR = 0;

#pragma PERSISTENT(FRAM_gud_TT_LTR)
double FRAM_gud_TT_LTR = 0;

void display_Value(unsigned long);
void clear_data();
void display_Batch();
void display_Total(unsigned long int);
void display_Flowrate(unsigned long int);
void displayScale(unsigned long);
void display_ScaleFator(unsigned long int);

unsigned char d1,d2,d3,d4,d5,d6,d7,d8;
unsigned char gui_dispParameter;
//unsigned char guc_SetupMode, guc_saveDataFlag;
unsigned int gui_PulseContForFlowRate=0,gucButtonPressTimer=0;

//float gf_VolperPulse;
//unsigned long gul_VolperPulse;


long int  setupConter=0;                  //variable counter used after pressing two key to enter in setup mode
unsigned char breakFlag=0,passmatch=0;

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer

    gui_dispParameter =1;
    guc_SetupMode=0;

    //  gul_VolperPulse = FRAM_guiVolperPulse;
//    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
    gul_VolperPulse = 158306;
    gf_VolperPulse = (float)gul_VolperPulse/10000;
    _nop();
    gf_VolperPulse = gf_VolperPulse/1000;
    _nop();


    // Configure all GPIO to Output Low
    P1OUT = 0x20;P2OUT = 0x00;P3OUT = 0x00;P4OUT = 0x00;
    P5OUT = 0x02;P6OUT = 0x00;P7OUT = 0x00;P8OUT = 0x00;

    P1DIR = 0x5F;P2DIR = 0xFF;P3DIR = 0xFF;P4DIR = 0xFF;
    P5DIR = 0xFD;P6DIR = 0xFF;P7DIR = 0xFF;P8DIR = 0xFF;



    // Configure XT1 oscillator
    P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins 323768KHz External Crystal
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag





////---------- Initialize Pulse Pin-----------------
    P1DIR &= ~(BIT7);                          // Configure P1.3 as pulled-up
    P1OUT |= BIT7;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT7;                          // P1.3 pull-up register enable
    P1IES = 0;
    P1IE = 0;
    P1IES |= BIT7;                          // P1.3 Hi/Low edge
    P1IE |= BIT7;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

////---------- Initialize button Pin-----------------
    P1DIR &= ~(BIT5);                          // Configure P1.3 as pulled-up
//    P1OUT |= BIT5;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT5;                          // P1.3 pull-up register enable
    P1IES &= ~BIT5;                          // P1.3 Hi/Low edge
    P1IE |= BIT5;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

    P5DIR &= ~(BIT2);                          // Configure P1.3 as pulled-up

//    // Configure GPIO
//       P1OUT &= ~BIT0;                         // Clear P1.0 output latch for a defined power-on state
//       P1DIR |= BIT0;                          // Set P1.0 to output direction



////------------------- Initiate Segment LCD ------------------------------------------------------
    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFC7;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM2,

    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected
    display_Batch();
////------------------- Initiate Segment LCD ------------------------------------------------------

    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

//    PMMCTL0_H = PMMPW_H;                                       // Open PMM Registers for write
//    PMMCTL0_L |= PMMREGOFF_L;                                  // and set PMMREGOFF

    init_Timer0_A1();


    P1IFG &= ~BIT7;                         // P1.3 IFG cleared
    P1IFG &= ~BIT5;                         // P1.3 IFG cleared

    __bis_SR_register(LPM4_bits | GIE); // Enter LPM3 w/interrupt

    while(1)
    {



        if(guc_SetupMode)
        {
            setup_Mode();
        }

        if((gui_dispParameter==1) && (guc_SetupMode==0))
        {
            display_Batch();
        }
        else if((gui_dispParameter==2) && (guc_SetupMode==0))
        {
            display_Total((unsigned long int)FRAM_gud_TT_LTR);
        }
        else if((gui_dispParameter==3) && (guc_SetupMode==0))
        {
            display_Flowrate(gul_FlowRateLPH);
        }

        if((gul_FlowRateLPH==0) && (guc_SetupMode==0))
        {
            if((gui_dispParameter==3) && (guc_SetupMode==0))
            {
                display_Flowrate(gul_FlowRateLPH);
            }
            TA0CTL=0;
            __bis_SR_register(LPM4_bits | GIE); // Enter LPM3 w/interrupt
            init_Timer0_A1();
            Timer0_A1_ON();
        }
        __no_operation();                   // For debugger
    }
}

// Port 1 interrupt service routine

void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
{
    _nop();

       __bic_SR_register_on_exit(LPM4_bits);   // Exit LPM3
//    if(P1IFG&BIT7)
    if(P1IFG == 0x80)
    {
        P1IFG &= ~BIT7;                         // Clear P1.3 IFG
        _nop();
        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
        FRAM_gud_CT_LTR = FRAM_gud_CT_LTR + gf_VolperPulse;
        FRAM_gud_TT_LTR = FRAM_gud_TT_LTR + gf_VolperPulse;
        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
        gui_PulseContForFlowRate++;
    }
//    else if(P1IFG&BIT5)
    else if(P1IFG == 0x20)
    {
//        P1IFG &= ~BIT5;                         // Clear P1.3 IFG
//        gucButtonPressFlag=1;
//        gui_dispParameter++;
//        if(gui_dispParameter>=4)
//        {
//            gui_dispParameter =1;
//        }

        _nop();
        setupConter=0;
        while(P1IN & BIT5)
        {
            _nop();
            if((gui_dispParameter==1) && (guc_SetupMode == 0))
            {
                setupConter++;
                if(setupConter>45000)
                {
                    _nop();


                    if (P5IN & BIT2)
                    {
                        setupConter=0;
                        guc_SetupMode=1;
                        display_ScaleFator(gul_VolperPulse);
                        breakFlag=1;
                        guc_SetupMode=1;
                        _no_operation();
                        break;
                    }
                    else if(setupConter>90000)
                    {
                        setupConter=0;
                        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
                        FRAM_gud_CT_LTR = 0;
                        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
                        display_Batch(FRAM_gud_CT_LTR);
                        _delay_cycles(10000);
                        breakFlag=1;
                        break;
                    }
//                    while(P1IN & BIT5);
//                    breakFlag=1;
//                    passmatch=1;
//                    break;
                }
            }
            else if((gui_dispParameter==1) && (guc_SetupMode == 1))
            {
                setupConter++;
                if(setupConter>22550)
                {
                    setupConter=0;
                    breakFlag=1;
                    guc_saveDataFlag=1;
                    break;
                }
            }
        }
        if( (breakFlag==0) && (guc_SetupMode==0))
        {
            gui_dispParameter++;
            if(gui_dispParameter>=4)
            {
                gui_dispParameter =1;
            }
        }
        else
        {
            breakFlag=0;
            gucButtonPressFlag=1;
        }
        P1IFG &= ~BIT5;                         // Clear P1.3 IFG
    }
    P1IFG=0;
}




void clear_data()
{
    LCDMEM[3] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[5] = 0x00;
    LCDMEM[6] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[9] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[11] = 0x00;
    LCDMEM[12] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[14] = 0x00;
}

void display_Batch()
{
    d8 =  (long)((FRAM_gud_CT_LTR*10))%10;

    d7 = ((long)FRAM_gud_CT_LTR%10);
    d6 = ((long)FRAM_gud_CT_LTR/10)%10;
    d5 = ((long)FRAM_gud_CT_LTR/100)%10;
    d4 = ((long)FRAM_gud_CT_LTR/1000)%10;
    d3 = ((long)FRAM_gud_CT_LTR/10000)%10;
    d2 = ((long)FRAM_gud_CT_LTR/100000)%10;
    d1 = ((long)FRAM_gud_CT_LTR/1000000);


    clear_data();

    //for display Batch LTR
    LCDMEM[14] = 0x40;
    LCDMEM[8] = 0x40;
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;

    if((long)FRAM_gud_CT_LTR<=9)
    {
       display_digit(d8,8);
       display_digit(d7,7);
    }
    else if((long)FRAM_gud_CT_LTR>9 && (long)FRAM_gud_CT_LTR<=99)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if((long)FRAM_gud_CT_LTR>99 && (long)FRAM_gud_CT_LTR<=999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);

    }
    else if((long)FRAM_gud_CT_LTR>999 && (long)FRAM_gud_CT_LTR<=9999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if((long)FRAM_gud_CT_LTR>9999 && (long)FRAM_gud_CT_LTR<=99999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if((long)FRAM_gud_CT_LTR>99999 && (long)FRAM_gud_CT_LTR<=999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if((long)FRAM_gud_CT_LTR>999999 && (long)FRAM_gud_CT_LTR<=9999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}

void display_Total(unsigned long int Total)
{
    d8 =  Total%10;
    d7 = (Total/10)%10;
    d6 = (Total/100)%10;
    d5 = (Total/1000)%10;
    d4 = (Total/10000)%10;
    d3 = (Total/100000)%10;
    d2 = (Total/1000000)%10;
    d1 = (Total/10000000);

    clear_data();
    //for display Total LTS
    LCDMEM[13] = 0x04;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Total);
}
void display_Flowrate(unsigned long int Flow)
{
    d8 =  Flow%10;
    d7 = (Flow/10)%10;
    d6 = (Flow/100)%10;
    d5 = (Flow/1000)%10;
    d4 = (Flow/10000)%10;
    d3 = (Flow/100000)%10;
    d2 = (Flow/1000000)%10;
    d1 = (Flow/10000000);

    if(Flow<=99999999)
    {
        clear_data();
    }

    //for display Flow Rate
    LCDMEM[11] = 0x40;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Flow);
}


void display_Value(unsigned long Value)
{
    if(Value<=9)
    {
        display_digit(d8,8);
    }
    else if(Value>9 && Value<=99)
    {
        display_digit(d8,8);
        display_digit(d7,7);
    }
    else if(Value>99 && Value<=999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if(Value>999 && Value<=9999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
    }
    else if(Value>9999 && Value<=99999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if(Value>99999 && Value<=999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if(Value>999999 && Value<=9999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if(Value>9999999 && Value<=99999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}

void display_ScaleFator(unsigned long int ScaleFactor)
{

    clear_data();

    //for display Flow Rate
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    displayScale(ScaleFactor);
}


void displayScale(unsigned long Value)
{
    display_digit(((Value%10)),8);
    display_digit(((Value/10)%10),7);
    display_digit(((Value/100)%10),6);
    display_digit(((Value/1000)%10),5);
    display_digit(((Value/10000)%10),4);
    display_digit(((Value/100000)%10),3);
    display_digit(((Value/1000000)%10),2);
    display_digit((Value/10000000),1);
}
