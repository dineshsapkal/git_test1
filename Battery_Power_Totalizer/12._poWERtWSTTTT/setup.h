/*
 * setup.h
 *
 *  Created on: 25-Dec-2017
 *      Author: Dell
 */

#ifndef SETUP_H_
#define SETUP_H_



extern unsigned char guc_SetupMode,guc_saveDataFlag;
extern float gf_VolperPulse;
extern unsigned long gul_VolperPulse;
extern unsigned long FRAM_guiVolperPulse;


//extern unsigned int guiMBR_3001,guiMBR_3002,guiMBR_3003;
//extern unsigned int guiMBR_4001,guiMBR_4002,guiMBR_4003;
//
//extern unsigned int long gul_tempCalulation,temp_gul_CT_LTR_IntegralPart;
//extern double gd_tempCalulation;

extern void setup_Mode();
extern void display_digit(unsigned char,unsigned char);


#endif /* SETUP_H_ */
