/*
 * segment_lcd.h
 *
 *  Created on: 11-Jan-2018
 *      Author: Dell
 */

#ifndef SEGMENT_LCD_H_
#define SEGMENT_LCD_H_


extern void display_Value(unsigned long);
extern void clear_data();
extern void display_Batch();
extern void display_Total(unsigned long int);
extern void displayScale(unsigned long);
extern void display_ScaleFator(unsigned long int);


#endif /* SEGMENT_LCD_H_ */
