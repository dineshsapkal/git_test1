/*
 * setup.h
 *
 *  Created on: 25-Dec-2017
 *      Author: Dell
 */

#ifndef SETUP_H_
#define SETUP_H_


extern double FRAM_gud_CT_LTR;
extern double FRAM_gud_TT_LTR;


extern unsigned char guc_SetupMode,guc_saveDataFlag;
extern float gf_VolperPulse;
extern unsigned long FRAM_guiVolperPulse;


extern unsigned char gucReset_CT_Flag,gucReset_TT_Flag;

extern unsigned char gucButtonPressFlag;

extern void wait();
extern void setup_Mode();
extern void display_digit(unsigned char,unsigned char);


#endif /* SETUP_H_ */
