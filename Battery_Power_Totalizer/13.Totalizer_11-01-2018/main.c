


#include "basic_requirement.h"


#pragma PERSISTENT(FRAM_gud_CT_LTR)
double FRAM_gud_CT_LTR = 0;

#pragma PERSISTENT(FRAM_gud_TT_LTR)
double FRAM_gud_TT_LTR = 0;




unsigned char gui_dispParameter;
unsigned int gui_PulseContForFlowRate=0;
long int  setupConter=0;                  //variable counter used after pressing two key to enter in setup mode
unsigned char breakFlag=0,passmatch=0;

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer

    gui_dispParameter =1;
    guc_SetupMode=0;

    //  gul_VolperPulse = FRAM_guiVolperPulse;
//    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
//    gul_VolperPulse = 158306;

//    SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
//    FRAM_guiVolperPulse = 12345678;
//    SYSCFG0 |= PFWP;
    gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
    _nop();
    gf_VolperPulse = gf_VolperPulse/1000;
    _nop();





    init_gpio();
    initiate_Oscillator();





////---------- Initialize Pulse Pin-----------------
    P1DIR &= ~(BIT7);                          // Configure P1.3 as pulled-up
    P1OUT |= BIT7;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT7;                          // P1.3 pull-up register enable
    P1IES = 0;
    P1IE = 0;
    P1IES |= BIT7;                          // P1.3 Hi/Low edge
    P1IE |= BIT7;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

////---------- Initialize button Pin-----------------
    P1DIR &= ~(BIT5);                          // Configure P1.3 as pulled-up
//    P1OUT |= BIT5;                          // Configure P1.3 as pulled-up
//    P1REN |= BIT5;                          // P1.3 pull-up register enable
    P1IES &= ~BIT5;                          // P1.3 Hi/Low edge
    P1IE |= BIT5;                           // P1.3 interrupt enabled
////---------- END Initialize Pulse Pin-----------------

    P5DIR &= ~(BIT2);                          // Configure P1.3 as pulled-up


////------------------- Initiate Segment LCD ------------------------------------------------------
    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFC7;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM2,

    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected
    display_Batch();
////------------------- Initiate Segment LCD ------------------------------------------------------

    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

//    PMMCTL0_H = PMMPW_H;                                       // Open PMM Registers for write
//    PMMCTL0_L |= PMMREGOFF_L;                                  // and set PMMREGOFF

    init_Timer0_A1();


    P1IFG &= ~BIT7;                         // P1.3 IFG cleared
    P1IFG &= ~BIT5;                         // P1.3 IFG cleared

    __bis_SR_register(GIE); // Enter LPM3 w/interrupt

    while(1)
    {
        if(guc_SetupMode)
        {
            setup_Mode();
        }
        else if(gucReset_CT_Flag)
        {
            gucReset_CT_Flag = 0;
            SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
            FRAM_gud_CT_LTR = 0;
            SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
            display_Batch();
            wait();
        }
        else if(gucReset_TT_Flag)
        {
            gucReset_TT_Flag = 0;
            SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
            FRAM_gud_TT_LTR = 0;
            SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
            display_Total((unsigned long int)FRAM_gud_TT_LTR);
            wait();
        }

        if((gui_dispParameter==1) && (guc_SetupMode==0))
        {
            display_Batch();
        }
        else if((gui_dispParameter==2) && (guc_SetupMode==0))
        {
            display_Total((unsigned long int)FRAM_gud_TT_LTR);
        }
        else if((gui_dispParameter==3) && (guc_SetupMode==0))
        {
            display_Flowrate(gul_FlowRateLPH);
        }

        if((gul_FlowRateLPH==0) && (guc_SetupMode==0))
        {
            if((gui_dispParameter==3) && (guc_SetupMode==0))
            {
                display_Flowrate(gul_FlowRateLPH);
            }
            TA0CTL=0;
//            P1OUT |=BIT6;
            __bis_SR_register(LPM4_bits | GIE); // Enter LPM3 w/interrupt
//            P1OUT &= ~BIT6;
            init_Timer0_A1();
            Timer0_A1_ON();
        }
        __no_operation();                   // For debugger
    }
}

// Port 1 interrupt service routine

void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
{
    _nop();

       __bic_SR_register_on_exit(LPM4_bits);   // Exit LPM3
//    if(P1IFG&BIT7)
    if(P1IFG == 0x80)
    {
        P1IFG &= ~BIT7;                         // Clear P1.3 IFG
        _nop();
        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
        FRAM_gud_CT_LTR = FRAM_gud_CT_LTR + gf_VolperPulse;
        FRAM_gud_TT_LTR = FRAM_gud_TT_LTR + gf_VolperPulse;
        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
        gui_PulseContForFlowRate++;
    }
//    else if(P1IFG&BIT5)
    else if(P1IFG == 0x20)
    {
        _nop();
        setupConter=0;
        while(P1IN & BIT5)
        {
            _nop();
            if((gui_dispParameter==1) && (guc_SetupMode == 0))
            {
                setupConter++;
                if(setupConter>45000)
                {
                    _nop();


                    if (P5IN & BIT2)
                    {
                        setupConter=0;
                        guc_SetupMode=1;
//                        display_ScaleFator(gul_VolperPulse);
                        breakFlag=1;
                        guc_SetupMode=1;
                        _no_operation();
                        break;
                    }
                    else if(setupConter>90000)
                    {
                        setupConter=0;
                        gucReset_CT_Flag = 1;
                        breakFlag=1;
                        break;
                    }
                }
            }
            else if((gui_dispParameter==1) && (guc_SetupMode == 1))
            {
                setupConter++;
                if(setupConter>22550)
                {
                    setupConter=0;
                    breakFlag=1;
                    guc_saveDataFlag=1;
                    break;
                }
            }
        }
        if( (breakFlag==0) && (guc_SetupMode==0))
        {
            gui_dispParameter++;
            if(gui_dispParameter>=4)
            {
                gui_dispParameter =1;
            }
        }
        else
        {
            breakFlag=0;
            gucButtonPressFlag=1;
        }
        P1IFG &= ~BIT5;                         // Clear P1.3 IFG
    }
    P1IFG=0;
}


