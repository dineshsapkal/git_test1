################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
captivate_app/CAPT_App.obj: ../captivate_app/CAPT_App.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/bin/cl430" -vmspx --code_model=small --data_model=small -O3 --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/driverlib" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/driverlib/MSP430FR2xx_4xx" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/mathlib" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/ADVANCED" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/BASE" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/COMM" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate_app" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate_config" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/include" --advice:power="none" --advice:hw_config=all --define=__MSP430FR2532__ --define=TARGET_IS_MSP430FR2XX_4XX -g --gcc --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="captivate_app/CAPT_App.d" --obj_directory="captivate_app" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

captivate_app/CAPT_BSP.obj: ../captivate_app/CAPT_BSP.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/bin/cl430" -vmspx --code_model=small --data_model=small -O3 --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/driverlib" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/driverlib/MSP430FR2xx_4xx" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/mathlib" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/ADVANCED" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/BASE" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate/COMM" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate_app" --include_path="F:/CCS_Codes/Battery_Power_Totalizer/CapButton_for_totalizer/captivate_config" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/include" --advice:power="none" --advice:hw_config=all --define=__MSP430FR2532__ --define=TARGET_IS_MSP430FR2XX_4XX -g --gcc --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="captivate_app/CAPT_BSP.d" --obj_directory="captivate_app" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


