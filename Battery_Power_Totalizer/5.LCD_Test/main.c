













/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************
//   MSP430FR413x Demo -  LCD_E, Display a string "123456" on LCD in LPM3.5 mode.
//
//   Description: Displays "123456" in sequence to the LCD display.
//                f(LCD) = 32768Hz/((7+1)*16) = 256Hz.
//                MSP430 works in LPM3.5 mode for ultra low power.
//                ACLK = default REFO ~32768Hz,
//                MCLK = SMCLK = default DCODIV ~1MHz.
//
//                MSP430FR4133
//             -----------------
//         /|\|                 |
//          | |              XIN|--
// GND      --|RST              |  ~32768Hz
//  |         |             XOUT|--
//  |--0.1uF--|R13              |
//  |--0.1uF--|R23      (L3)COM3|----------------|
//  |--0.1uF--|R33      (L2)COM2|---------------||
//          --|LCDC2    (L1)COM1|--------------|||
//     4.7uF  |         (L0)COM0|-------------||||
//          --|LCDC1            |    -------------
//            |           L4~L39|---| 1 2 3 4 5 6 |
//            |   except L27~L35|    -------------
//            |                 |       TI LCD
//            |                 |
//
//  LCD pin - Port Pin Map
//  LCD pin   G6021_LineX
//    1         L8  (P3.0)
//    2         L9  (P3.1)
//    3         L10 (P3.2)
//    4         L11 (P3.3)
//    5         L12 (P3.4)
//    6         L13 (P3.5)
//    7         L14 (P3.6)
//    8         L15 (P3.7)
//    9         L16 (P6.0)
//    10        L17 (P6.1)
//    11        L18 (P6.2)
//    12        L19 (P6.3)
//    13        L20 (P6.4)
//    14        L21 (P6.5)
//    15        L22 (P6.6)
//    16        L23 (P6.7)
//    17        L4  (P7.4)
//    18        L5  (P7.5)
//    19        L6  (P7.6)
//    20        L7  (P7.7)
//    21        L3  (P7.3)
//    22        L2  (P7.2)
//    23        L1  (P7.1)
//    24        L0  (P7.0)
//    25        -
//    26        -
//    27        -
//    28        -
//    29        -
//    30        -
//    31        -
//    32        L24 (P2.0)
//    33        L25 (P2.1)
//    34        L26 (P2.2)
//    35        L36 (P5.4)
//    36        L37 (P5.5)
//    37        L38 (P5.6)
//    38        L39 (P5.7)
//
//  Cen Fang
//  Wei Zhao
//  Texas Instruments Inc.
//  Oct 2013
//  Built with IAR Embedded Workbench v5.60 & Code Composer Studio v5.5
//******************************************************************************
#include <msp430.h>

#define pos1 4                                                 // Digit A1 - L4
#define pos2 6                                                 // Digit A2 - L6
#define pos3 8                                                 // Digit A3 - L8
#define pos4 10                                                // Digit A4 - L10
#define pos5 2                                                 // Digit A5 - L2
#define pos6 18                                                // Digit A6 - L18

const char digit[10] =
{
    0xFC,                                                      // "0"
    0x60,                                                      // "1"
    0xDB,                                                      // "2"
    0xF3,                                                      // "3"
    0x67,                                                      // "4"
    0xB7,                                                      // "5"
    0xBF,                                                      // "6"
    0xE4,                                                      // "7"
    0xFF,                                                      // "8"
    0xF7                                                       // "9"
};

int main( void )
{
     WDTCTL = WDTPW | WDTHOLD;                                  // Stop watchdog timer

    // Configure XT1 oscillator
    P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
    CSCTL6 = (CSCTL6 & ~(XT1DRIVE_3)) | XT1DRIVE_2;            // Higher drive strength and current consumption for XT1 oscillator


    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

    // Configure LCD pins
    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFff;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;                                         // L0~L26 & L36~L39 pins selected

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2: COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM3,



    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 4-mux selected
//    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 4-mux selected

    PMMCTL0_H = PMMPW_H;                                       // Open PMM Registers for write
    PMMCTL0_L |= PMMREGOFF_L;                                  // and set PMMREGOFF

//    __bis_SR_register(LPM3_bits | GIE);                        // Enter LPM3.5
    __no_operation();                                          // For debugger.
    while(1)
    {

                        //0
                    LCDMEM[3] = 0xFF;
                    LCDMEM[4] = 0xFF;

                    LCDMEM[4] = 0xFF;
                    LCDMEM[5] = 0xFF;

                    LCDMEM[6] = 0xFF;
                    LCDMEM[7] = 0xFF;

                    LCDMEM[7] = 0xFF;
                    LCDMEM[8] = 0xFF;

                    LCDMEM[9] = 0xFF;
                    LCDMEM[10] = 0xFF;

                    LCDMEM[10] = 0xFF;
                    LCDMEM[11] = 0xFF;

                    LCDMEM[12] = 0xFF;
                    LCDMEM[13] = 0xFF;

                    LCDMEM[13] = 0xFF;
                    LCDMEM[14] = 0xFF;


                                _delay_cycles(500000);

//                    LCDMEM[3] = 0x00;
//                       LCDMEM[4] = 0x00;
//                       LCDMEM[4] = 0x00;
//                       LCDMEM[5] = 0x00;
//                       LCDMEM[6] = 0x00;
//                       LCDMEM[7] = 0x00;
//                       LCDMEM[7] = 0x00;
//                       LCDMEM[8] = 0x00;
//                       LCDMEM[9] = 0x00;
//                       LCDMEM[10] = 0x00;
//                       LCDMEM[10] = 0x00;
//                       LCDMEM[11] = 0x00;
//                       LCDMEM[12] = 0x00;
//                       LCDMEM[13] = 0x00;
//                       LCDMEM[13] = 0x00;
//                       LCDMEM[14] = 0x00;
//                       _delay_cycles(500000);

//        // Digit_8
//
//
//                //0
//            LCDMEM[3] = 0x53;
//            LCDMEM[4] = 0x03;
//
//            LCDMEM[4] = 0x33;
//            LCDMEM[5] = 0x35;
//
//            LCDMEM[6] = 0x53;
//            LCDMEM[7] = 0x03;
//
//            LCDMEM[7] = 0x33;
//            LCDMEM[8] = 0x35;
//
//            LCDMEM[9] = 0x53;
//            LCDMEM[10] = 0x03;
//
//            LCDMEM[10] = 0x33;
//            LCDMEM[11] = 0x35;
//
//            LCDMEM[12] = 0x53;
//            LCDMEM[13] = 0x03;
//
//            LCDMEM[13] = 0x33;
//            LCDMEM[14] = 0x35;
//            _delay_cycles(500000);
//
//
//                //1
//            LCDMEM[3] = 0x03;
//            LCDMEM[4] = 0x00;
//
//            LCDMEM[4] = 0x30;
//            LCDMEM[5] = 0x00;
//
//            LCDMEM[6] = 0x03;
//            LCDMEM[7] = 0x00;
//
//            LCDMEM[7] = 0x30;
//            LCDMEM[8] = 0x00;
//
//            LCDMEM[9] = 0x03;
//            LCDMEM[10] = 0x00;
//
//            LCDMEM[10] = 0x30;
//            LCDMEM[11] = 0x00;
//
//            LCDMEM[12] = 0x03;
//            LCDMEM[13] = 0x00;
//
//            LCDMEM[13] = 0x30;
//            LCDMEM[14] = 0x00;
//            _delay_cycles(500000);
//
//            //2
//            LCDMEM[3] = 0x71;
//            LCDMEM[4] = 0x02;
//
//            LCDMEM[4] = 0x12;
//            LCDMEM[5] = 0x27;
//
//            LCDMEM[6] = 0x71;
//            LCDMEM[7] = 0x02;
//
//            LCDMEM[7] = 0x12;
//            LCDMEM[8] = 0x27;
//
//            LCDMEM[9] = 0x71;
//            LCDMEM[10] = 0x02;
//
//            LCDMEM[10] = 0x12;
//            LCDMEM[11] = 0x27;
//
//            LCDMEM[12] = 0x71;
//            LCDMEM[13] = 0x02;
//
//            LCDMEM[13] = 0x12;
//            LCDMEM[14] = 0x27;
//            _delay_cycles(500000);
//
//            //3
//            LCDMEM[3] = 0x73;
//            LCDMEM[4] = 0x00;
//
//            LCDMEM[4] = 0x30;
//            LCDMEM[5] = 0x07;
//
//            LCDMEM[6] = 0x73;
//            LCDMEM[7] = 0x00;
//
//            LCDMEM[7] = 0x30;
//            LCDMEM[8] = 0x07;
//
//            LCDMEM[9] = 0x73;
//            LCDMEM[10] = 0x00;
//
//            LCDMEM[10] = 0x30;
//            LCDMEM[11] = 0x07;
//
//            LCDMEM[12] = 0x73;
//            LCDMEM[13] = 0x00;
//
//            LCDMEM[13] = 0x30;
//            LCDMEM[14] = 0x07;
//            _delay_cycles(500000);
//
//            //4
//            LCDMEM[3] = 0x23;
//            LCDMEM[4] = 0x01;
//
//            LCDMEM[4] = 0x31;
//            LCDMEM[5] = 0x12;
//
//            LCDMEM[6] = 0x23;
//            LCDMEM[7] = 0x01;
//
//            LCDMEM[7] = 0x31;
//            LCDMEM[8] = 0x12;
//
//            LCDMEM[9] = 0x23;
//            LCDMEM[10] = 0x01;
//
//            LCDMEM[10] = 0x31;
//            LCDMEM[11] = 0x12;
//
//            LCDMEM[12] = 0x23;
//            LCDMEM[13] = 0x01;
//
//            LCDMEM[13] = 0x31;
//            LCDMEM[14] = 0x12;
//            _delay_cycles(500000);
//
//
//            //5
//            LCDMEM[3] = 0x72;
//            LCDMEM[4] = 0x01;
//
//            LCDMEM[4] = 0x21;
//            LCDMEM[5] = 0x17;
//
//            LCDMEM[6] = 0x72;
//            LCDMEM[7] = 0x01;
//
//            LCDMEM[7] = 0x21;
//            LCDMEM[8] = 0x17;
//
//            LCDMEM[9] = 0x72;
//            LCDMEM[10] = 0x01;
//
//            LCDMEM[10] = 0x21;
//            LCDMEM[11] = 0x17;
//
//            LCDMEM[12] = 0x72;
//            LCDMEM[13] = 0x01;
//
//            LCDMEM[13] = 0x21;
//            LCDMEM[14] = 0x17;
//            _delay_cycles(500000);
//
//
//            //6
//            LCDMEM[3] = 0x72;
//            LCDMEM[4] = 0x03;
//
//            LCDMEM[4] = 0x23;
//            LCDMEM[5] = 0x37;
//
//            LCDMEM[6] = 0x72;
//            LCDMEM[7] = 0x03;
//
//            LCDMEM[7] = 0x23;
//            LCDMEM[8] = 0x37;
//
//            LCDMEM[9] = 0x72;
//            LCDMEM[10] = 0x03;
//
//            LCDMEM[10] = 0x23;
//            LCDMEM[11] = 0x37;
//
//            LCDMEM[12] = 0x72;
//            LCDMEM[13] = 0x03;
//
//            LCDMEM[13] = 0x23;
//            LCDMEM[14] = 0x37;
//            _delay_cycles(500000);
//
//
//            //7
//            LCDMEM[3] = 0x13;
//            LCDMEM[4] = 0x00;
//
//            LCDMEM[4] = 0x30;
//            LCDMEM[5] = 0x01;
//
//            LCDMEM[6] = 0x13;
//            LCDMEM[7] = 0x00;
//
//            LCDMEM[7] = 0x30;
//            LCDMEM[8] = 0x01;
//
//            LCDMEM[9] = 0x13;
//            LCDMEM[10] = 0x00;
//
//            LCDMEM[10] = 0x30;
//            LCDMEM[11] = 0x01;
//
//            LCDMEM[12] = 0x13;
//            LCDMEM[13] = 0x00;
//
//            LCDMEM[13] = 0x30;
//            LCDMEM[14] = 0x01;
//            _delay_cycles(500000);
//
//
//            //8
//            LCDMEM[3] = 0x73;
//            LCDMEM[4] = 0x03;
//
//            LCDMEM[4] = 0x33;
//            LCDMEM[5] = 0x37;
//
//            LCDMEM[6] = 0x73;
//            LCDMEM[7] = 0x03;
//
//            LCDMEM[7] = 0x33;
//            LCDMEM[8] = 0x37;
//
//            LCDMEM[9] = 0x73;
//            LCDMEM[10] = 0x03;
//
//            LCDMEM[10] = 0x33;
//            LCDMEM[11] = 0x37;
//
//            LCDMEM[12] = 0x73;
//            LCDMEM[13] = 0x03;
//
//            LCDMEM[13] = 0x33;
//            LCDMEM[14] = 0x37;
//            _delay_cycles(500000);
//
//
//            //9
//            LCDMEM[3] = 0x73;
//            LCDMEM[4] = 0x01;
//
//            LCDMEM[4] = 0x31;
//            LCDMEM[5] = 0x17;
//
//            LCDMEM[6] = 0x73;
//            LCDMEM[7] = 0x01;
//
//            LCDMEM[7] = 0x31;
//            LCDMEM[8] = 0x17;
//
//            LCDMEM[9] = 0x73;
//            LCDMEM[10] = 0x01;
//
//            LCDMEM[10] = 0x31;
//            LCDMEM[11] = 0x17;
//
//            LCDMEM[12] = 0x73;
//            LCDMEM[13] = 0x01;
//
//            LCDMEM[13] = 0x31;
//            LCDMEM[14] = 0x17;
//            _delay_cycles(500000);




    }

}






//
////******************************************************************************
//#include <msp430.h>
//
// void display_digit(unsigned char,unsigned char);
// void display_Value(unsigned long);
//
// void display_Batch();
// void display_Total();
// void display_Flowrate();
//
//
//
//int main( void )
//{
//    unsigned long len;
//     WDTCTL = WDTPW | WDTHOLD;                                  // Stop watchdog timer
//
//    // Configure XT1 oscillator
//    P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins
//    do
//    {
//        CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
//        SFRIFG1 &= ~OFIFG;
//    }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
//    CSCTL6 = (CSCTL6 & ~(XT1DRIVE_3)) | XT1DRIVE_2;            // Higher drive strength and current consumption for XT1 oscillator
//
//
//    // Disable the GPIO power-on default high-impedance mode
//    // to activate previously configured port settings
//    PM5CTL0 &= ~LOCKLPM5;
//
//    // Configure LCD pins
//    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected
//
//    LCDPCTL0 = 0xFFC7;
//    LCDPCTL1 = 0x3FFF;
//    LCDPCTL2 = 0x0000;
//
//    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk
//
//    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
//    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);
//
//    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory
//
//    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
//    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
//    LCDCSSEL2 = 0x0000;
//
//    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
//    LCDM1 = 0x04;                                            // L2 = COM2,
//
//
//
//    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected
//
//    __no_operation();                                          // For debugger.
//
//
//
//
//    while(1)
//    {
//
//        for(len=9999999;len<=99999999;len++)
//        {
////            display_digit(len,8);
////            display_digit(len,7);
////            display_digit(len,6);
////            display_digit(len,5);
////            display_digit(len,4);
////            display_digit(len,3);
////            display_digit(len,2);
////            display_digit(len,1);
////            display_Value(len);
////            _delay_cycles(100000);
//            display_Batch();
//            _delay_cycles(500000);
//            display_Total();
//            _delay_cycles(500000);
//            display_Flowrate();
//            _delay_cycles(500000);
//        }
//    }
//
//}
//
//
//void display_digit(unsigned char dispNum,unsigned char pos)
//{
//   if(pos==8)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[3] = 0x53;
//                   LCDMEM[4] = 0x03;
//                   break;
//           case 1:
//                   LCDMEM[3] = 0x03;
//                   LCDMEM[4] = 0x00;
//                   break;
//           case 2:
//                   LCDMEM[3] = 0x71;
//                   LCDMEM[4] = 0x02;
//                   break;
//           case 3:
//                   LCDMEM[3] = 0x73;
//                   LCDMEM[4] = 0x00;
//                   break;
//           case 4:
//                   LCDMEM[3] = 0x23;
//                   LCDMEM[4] = 0x01;
//                   break;
//           case 5:
//                   LCDMEM[3] = 0x72;
//                   LCDMEM[4] = 0x01;
//                   break;
//           case 6:
//                   LCDMEM[3] = 0x72;
//                   LCDMEM[4] = 0x03;
//                   break;
//           case 7:
//                   LCDMEM[3] = 0x13;
//                   LCDMEM[4] = 0x00;
//                   break;
//           case 8:
//                   LCDMEM[3] = 0x73;
//                   LCDMEM[4] = 0x03;
//                   break;
//           case 9:
//                   LCDMEM[3] = 0x73;
//                   LCDMEM[4] = 0x01;
//                   break;
//       }
//   }
//   else if(pos==7)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x35;
//                   break;
//           case 1:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x00;
//                   break;
//           case 2:
//                   LCDMEM[4] = LCDMEM[4] | 0x10;
//                   LCDMEM[5] = 0x27;
//                   break;
//           case 3:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x07;
//                   break;
//           case 4:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x12;
//                   break;
//           case 5:
//                   LCDMEM[4] = LCDMEM[4] | 0x20;
//                   LCDMEM[5] = 0x17;
//                   break;
//           case 6:
//                   LCDMEM[4] = LCDMEM[4] | 0x20;
//                   LCDMEM[5] = 0x37;
//                   break;
//           case 7:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x01;
//                   break;
//           case 8:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x37;
//                   break;
//           case 9:
//                   LCDMEM[4] = LCDMEM[4] | 0x30;
//                   LCDMEM[5] = 0x17;
//                   break;
//       }
//   }
//   else  if(pos==6)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[6] = 0x53;
//                   LCDMEM[7] = 0x03;
//                   break;
//           case 1:
//                   LCDMEM[6] = 0x03;
//                   LCDMEM[7] = 0x00;
//                   break;
//           case 2:
//                   LCDMEM[6] = 0x71;
//                   LCDMEM[7] = 0x02;
//                   break;
//           case 3:
//                   LCDMEM[6] = 0x73;
//                   LCDMEM[7] = 0x00;
//                   break;
//           case 4:
//                   LCDMEM[6] = 0x23;
//                   LCDMEM[7] = 0x01;
//                   break;
//           case 5:
//                   LCDMEM[6] = 0x72;
//                   LCDMEM[7] = 0x01;
//                   break;
//           case 6:
//                   LCDMEM[6] = 0x72;
//                   LCDMEM[7] = 0x03;
//                   break;
//           case 7:
//                   LCDMEM[6] = 0x13;
//                   LCDMEM[7] = 0x00;
//                   break;
//           case 8:
//                   LCDMEM[6] = 0x73;
//                   LCDMEM[7] = 0x03;
//                   break;
//           case 9:
//                   LCDMEM[6] = 0x73;
//                   LCDMEM[7] = 0x01;
//                   break;
//       }
//   }
//   else  if(pos==5)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x35;
//                   break;
//           case 1:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x00;
//                   break;
//           case 2:
//                   LCDMEM[7] = LCDMEM[7] | 0x10;
//                   LCDMEM[8] = LCDMEM[8] | 0x27;
//                   break;
//           case 3:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x07;
//                   break;
//           case 4:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x12;
//                   break;
//           case 5:
//                   LCDMEM[7] = LCDMEM[7] | 0x20;
//                   LCDMEM[8] = LCDMEM[8] | 0x17;
//                   break;
//           case 6:
//                   LCDMEM[7] = LCDMEM[7] | 0x20;
//                   LCDMEM[8] = LCDMEM[8] | 0x37;
//                   break;
//           case 7:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x01;
//                   break;
//           case 8:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x37;
//                   break;
//           case 9:
//                   LCDMEM[7] = LCDMEM[7] | 0x30;
//                   LCDMEM[8] = LCDMEM[8] | 0x17;
//                   break;
//       }
//   }
//   else  if(pos==4)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[9] = 0x53;
//                   LCDMEM[10] = 0x03;
//                   break;
//           case 1:
//                   LCDMEM[9] = 0x03;
//                   LCDMEM[10] = 0x00;
//                   break;
//           case 2:
//                   LCDMEM[9] = 0x71;
//                   LCDMEM[10] = 0x02;
//                   break;
//           case 3:
//                   LCDMEM[9] = 0x73;
//                   LCDMEM[10] = 0x00;
//                   break;
//           case 4:
//                   LCDMEM[9] = 0x23;
//                   LCDMEM[10] = 0x01;
//                   break;
//           case 5:
//                   LCDMEM[9] = 0x72;
//                   LCDMEM[10] = 0x01;
//                   break;
//           case 6:
//                   LCDMEM[9] = 0x72;
//                   LCDMEM[10] = 0x03;
//                   break;
//           case 7:
//                   LCDMEM[9] = 0x13;
//                   LCDMEM[10] = 0x00;
//                   break;
//           case 8:
//                   LCDMEM[9] = 0x73;
//                   LCDMEM[10] = 0x03;
//                   break;
//           case 9:
//                   LCDMEM[9] = 0x73;
//                   LCDMEM[10] = 0x01;
//                   break;
//       }
//   }
//   else  if(pos==3)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x35;
//                   break;
//           case 1:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x00;
//                   break;
//           case 2:
//               _nop();
//                   LCDMEM[10] = LCDMEM[10] | 0x10;
//                   LCDMEM[11] = LCDMEM[11] | 0x27;
//                   break;
//           case 3:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x07;
//                   break;
//           case 4:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x12;
//                   break;
//           case 5:
//                   LCDMEM[10] = LCDMEM[10] | 0x20;
//                   LCDMEM[11] = LCDMEM[11] | 0x17;
//                   break;
//           case 6:
//                   LCDMEM[10] = LCDMEM[10] | 0x20;
//                   LCDMEM[11] = LCDMEM[11] | 0x37;
//                   break;
//           case 7:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x01;
//                   break;
//           case 8:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x37;
//                   break;
//           case 9:
//                   LCDMEM[10] = LCDMEM[10] | 0x30;
//                   LCDMEM[11] = LCDMEM[11] | 0x17;
//                   break;
//       }
//   }
//   else  if(pos==2)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[12] = 0x53;
//                   LCDMEM[13] = LCDMEM[13] | 0x03;
//                   break;
//           case 1:
//                   LCDMEM[12] = 0x03;
//                   LCDMEM[13] = LCDMEM[13] | 0x00;
//                   break;
//           case 2:
//                   LCDMEM[12] = 0x71;
//                   LCDMEM[13] = LCDMEM[13] | 0x02;
//                   break;
//           case 3:
//                   LCDMEM[12] = 0x73;
//                   LCDMEM[13] = LCDMEM[13] | 0x00;
//                   break;
//           case 4:
//                   LCDMEM[12] = 0x23;
//                   LCDMEM[13] = LCDMEM[13] | 0x01;
//                   break;
//           case 5:
//                   LCDMEM[12] = 0x72;
//                   LCDMEM[13] = LCDMEM[13] | 0x01;
//                   break;
//           case 6:
//                   LCDMEM[12] = 0x72;
//                   LCDMEM[13] = LCDMEM[13] | 0x03;
//                   break;
//           case 7:
//                   LCDMEM[12] = 0x13;
//                   LCDMEM[13] = LCDMEM[13] | 0x00;
//                   break;
//           case 8:
//                   LCDMEM[12] = 0x73;
//                   LCDMEM[13] = LCDMEM[13] | 0x03;
//                   break;
//           case 9:
//                   LCDMEM[12] = 0x73;
//                   LCDMEM[13] = LCDMEM[13] | 0x01;
//                   break;
//       }
//   }
//   else  if(pos==1)
//   {
//       switch(dispNum)
//       {
//           case 0:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x35;
//                   break;
//           case 1:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x00;
//                   break;
//           case 2:
//                   LCDMEM[13] = LCDMEM[13] | 0x10;
//                   LCDMEM[14] = LCDMEM[14] | 0x27;
//                   break;
//           case 3:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x07;
//                   break;
//           case 4:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x12;
//                   break;
//           case 5:
//                   LCDMEM[13] = LCDMEM[13] | 0x20;
//                   LCDMEM[14] = LCDMEM[14] | 0x17;
//                   break;
//           case 6:
//                   LCDMEM[13] = LCDMEM[13] | 0x20;
//                   LCDMEM[14] = LCDMEM[14] | 0x37;
//                   break;
//           case 7:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x01;
//                   break;
//           case 8:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x37;
//                   break;
//           case 9:
//                   LCDMEM[13] = LCDMEM[13] | 0x30;
//                   LCDMEM[14] = LCDMEM[14] | 0x17;
//                   break;
//       }
//   }
//
//}
//
//
//
//void display_Value(unsigned long Value)
//{
//    if(Value<=9)
//    {
//        display_digit(((Value%10)),8);
//    }
//    else if(Value>9 && Value<=99)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//    }
//    else if(Value>99 && Value<=999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//    }
//    else if(Value>999 && Value<=9999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//        display_digit(((Value/1000)%10),5);
//    }
//    else if(Value>9999 && Value<=99999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//        display_digit(((Value/1000)%10),5);
//    }
//    else if(Value>99999 && Value<=999999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//        display_digit(((Value/1000)%10),5);
//        display_digit(((Value/10000)%10),4);
//        display_digit(((Value/100000)%10),3);
//    }
//    else if(Value>999999 && Value<=9999999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//        display_digit(((Value/1000)%10),5);
//        display_digit(((Value/10000)%10),4);
//        display_digit(((Value/100000)%10),3);
//        display_digit(((Value/1000000)%10),2);
//    }
//    else if(Value>9999999 && Value<=99999999)
//    {
//        display_digit(((Value%10)),8);
//        display_digit(((Value/10)%10),7);
//        display_digit(((Value/100)%10),6);
//        display_digit(((Value/1000)%10),5);
//        display_digit(((Value/10000)%10),4);
//        display_digit(((Value/100000)%10),3);
//        display_digit(((Value/1000000)%10),2);
//        display_digit((Value/10000000),1);
//    }
//}
//
//
//void display_Batch()
//{
//    //for display Batch LTR
//    LCDMEM[14] = 0x40;
//    LCDMEM[8] = 0x40;
//    LCDMEM[11] = 0x00;
//    LCDMEM[13] = 0x00;
//
//    display_Value(12345678);
//}
//void display_Total()
//{
//    //for display Total LTS
//    LCDMEM[13] = 0x04;
//    LCDMEM[8] = 0x00;
//    LCDMEM[14] = 0x00;
//
//    display_Value(5678);
//}
//void display_Flowrate()
//{
//
//    //for display Flow Rate
//    LCDMEM[11] = 0x40;
//    LCDMEM[13] = 0x00;
//    LCDMEM[8] = 0x00;
//    LCDMEM[14] = 0x00;
//
//    display_Value(95686);
//}
//
//
//
//
