/*
 * keys.h
 *
 *  Created on: 15-Feb-2017
 *      Author: Dell
 */

#ifndef KEYS_H_
#define KEYS_H_




//------------------ defining the key return value ------------------
#define NO_KEY				        0
#define SCROLL_KEY      	        1
#define PASSWORD_KEY			    2
#define SAVE_DIGIT                  3



//--------------------------------------------------------------------

//-------------- Defining the PORT pin for Key -----------------------------
#define KBD_SCROLL_PIN_READ					P5IN
#define KBD_PASSWORD_PIN_READ				P5IN


// defining the SCROLL key
#define KBD_SCROLL_PIN_PORT				P5OUT
#define KBD_SCROLL_PIN_PORT_SEL0		P5SEL0
#define KBD_SCROLL_PIN_PORT_DIR     	P5DIR
#define KBD_SCROLL_PIN	    			BIT1
#define KBD_SCROLL_PIN_PULLDN   		P5REN


// defining the INC key
#define KBD_PASSWORD_PIN_PORT				P5OUT
#define KBD_PASSWORD_PIN_PORT_SEL0			P5SEL0
#define KBD_PASSWORD_PIN_PORT_DIR		    P5DIR
#define KBD_PASSWORD_PIN					BIT2
#define KBD_PASSWORD_PIN_PULLDN			    P5REN

//---------------------------------------------------------------------------------------

//defining global variable
extern unsigned int guiKeyDebounce,guiScreenDisplayTimer;


//--- defining the function prototype --------------
extern void Init_Keys();
extern int detectKey();




#endif /* KEYS_H_ */
