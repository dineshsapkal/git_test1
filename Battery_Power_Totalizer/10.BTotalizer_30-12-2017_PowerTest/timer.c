/*
 * timer.c
 *
 *  Created on: 03-Jan-2017
 *      Author: Dell
 */

#include "basic_requirement.h"


unsigned int guiKeyDebounce,guiScreenDisplayTimer;		//counter to detect key

unsigned int long gul_5ms_Count;
unsigned int long gul_5ms_Count_dummy;

double gd_FlowRate[10] ={0},gd_FlowRateAvg,gd_FlowRateAvg_1;
unsigned int guiIndex;
unsigned long int gul_FlowRateLPH;
double gd_FlowRateLPH ;

unsigned char gucFirstTime=1,len;


/*****************************************************************************************************
Function   :  init_Timer0_A1
 Description:  This Function used to Initialize the Timer for 5mSec
 Parameters :  NONE
 Returns    :  NONE
 Remarks    :
*******************************************************************************************************/
void init_Timer0_A1()
{
	  TA0CCR0 = 164;
	  TA0CTL = TASSEL__ACLK | MC_0 | TACLR | TAIE; // SMCLK, count mode, clear TAR
      	  	  	  	  	  	  	  	  	  	  	  	  	  	  // enable interrupt
}


void Timer0_A1_ON()
{
    TA0CCR0 = 164;
    TA0CTL |= MC__UP ;
}

void Timer0_A1_OFF()
{
    TA0CTL |= MC_0 ;
}
/*****************************************************************************************************
Function   :  init_Timer1_A3
 Description:  This Function used to Initialize the Timer for 1mSec
 Parameters :  NONE
 Returns    :  NONE
 Remarks    :
*******************************************************************************************************/
void init_Timer1_A3()
{
//	 TA1CCR0 = 50000;
//	 TA1CTL = TASSEL__SMCLK|ID_3 | MC_0 | TACLR | TAIE; // SMCLK, count mode, clear TAR

    TA1CCR0 = 32;
    TA1CTL = TASSEL__ACLK | MC__UP | TACLR | TAIE; // SMCLK, count mode, clear TAR
	                                                               // enable interrupt
}
/*****************************************************************************************************
 Function   :  TIMER0_A1_ISR
 Description:  This Function used to count the variables
 Parameters :  NONE
 Returns    :  NONE
 Remarks    :
*******************************************************************************************************/
void __attribute__ ((interrupt(TIMER0_A1_VECTOR))) TIMER0_A1_ISR (void)
{
  switch(__even_in_range(TA0IV, TA0IV_TAIFG))
  {
    case TA0IV_NONE:   break;               // No interrupt
    case TA0IV_TACCR1: break;               // CCR1 not used
    case TA0IV_TACCR2: break;               // CCR2 not used
    case TA0IV_3:      break;               // reserved
    case TA0IV_4:      break;               // reserved
    case TA0IV_5:      break;               // reserved
    case TA0IV_6:      break;               // reserved
    case TA0IV_TAIFG:                       // overflow
//        __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM3
                        gul_5ms_Count++;

                        if(gul_5ms_Count>=400)
                        {
//                            gul_5ms_Count = 0;            //changes done @27-11-2017
                            if(gucFirstTime == 1)
                            {
                                if(gui_PulseContForFlowRate!=0)     //changes done @27-11-2017
                                {
                                    gul_5ms_Count = 0;
                                }
                                gd_FlowRate[guiIndex] = gui_PulseContForFlowRate * gf_VolperPulse;

                                gd_FlowRateAvg=0;
                                for(len=0;len<=guiIndex;len++)
                                {
                                    gd_FlowRateAvg = gd_FlowRateAvg + gd_FlowRate[len];
                                }
                                gd_FlowRateAvg_1=0;
//                                gf_FlowRateAvg = gf_FlowRateAvg + gf_FlowRate[guiIndex];
                                gd_FlowRateAvg_1 = ((float)gd_FlowRateAvg/guiIndex);

                                gul_FlowRateLPH = 0;
                                gul_FlowRateLPH = gd_FlowRateAvg_1 * 1800;
                                guiIndex++;
                                gui_PulseContForFlowRate = 0;

                                if(guiIndex>10)
                                {
                                    gucFirstTime=0;
                                    guiIndex = 0;
                                    gui_PulseContForFlowRate = 0;
                                }
                            }
                            else
                            {
                                if(gui_PulseContForFlowRate!=0) //changes done @27-11-2017
                                {
                                    gul_5ms_Count = 0;
                                }
                                gd_FlowRate[guiIndex] = gui_PulseContForFlowRate * gf_VolperPulse;
                                guiIndex++;
                                gui_PulseContForFlowRate = 0;



                                gd_FlowRateAvg=0;
                                for(len=0;len<=9;len++)
                                {
                                    gd_FlowRateAvg = gd_FlowRateAvg + gd_FlowRate[len];
                                }
                                gd_FlowRateAvg_1=0;
                                gd_FlowRateAvg_1 = (gd_FlowRateAvg/10.00);
                                gul_FlowRateLPH = 0;
                                gul_FlowRateLPH = gd_FlowRateAvg_1 * 1800;



                                if(guiIndex>10)
                                {
                                    guiIndex = 0;
                                    gul_5ms_Count = 0;
                                    gui_PulseContForFlowRate = 0;
                                }
                            }
                            _nop();
                        }
                        break;
    default: break;
  }
}
void __attribute__ ((interrupt(TIMER1_A1_VECTOR))) TIMER1_A1_ISR (void)
{

  switch(__even_in_range(TA1IV, TA1IV_TAIFG))
    {
      case TA1IV_NONE:   break;               // No interrupt
      case TA1IV_TACCR1: break;               // CCR1 not used
      case TA1IV_TACCR2: break;               // CCR2 not used
      case TA1IV_3:      break;               // reserved
      case TA1IV_4:      break;               // reserved
      case TA1IV_5:      break;               // reserved
      case TA1IV_6:      break;               // reserved
      case TA1IV_TAIFG:                       // overflow
//          __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM3
                  guiKeyDebounce++;
                  guiScreenDisplayTimer++;
      	  	  	  	  break;
      default: break;
    }
}
