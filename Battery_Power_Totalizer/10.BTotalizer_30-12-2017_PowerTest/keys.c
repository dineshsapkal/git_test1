/*
 * keys.c
 *
 *  Created on: 15-Feb-2017
 *      Author: Dell
 */


#include "basic_requirement.h"

unsigned char gcArrPASSWORD[4]={0,0,0,0};

unsigned int lastgetKey=0xff;		//variable used to save last key pressed
unsigned int getKey=0xff;			//variable used to save pressed key


int setupConter=0;                  //variable counter used after pressing two key to enter in setup mode

/*****************************************************************************************************
 Function   :  Init_Keys
 Description:  This Function used to Initialize the Keys
 Parameters :  NONE
 Returns    :  NONE
 Remarks    :
*******************************************************************************************************/
void Init_Keys()
{

	KBD_SCROLL_PIN_PORT_SEL0 &= ~KBD_SCROLL_PIN;			// i/o function selected, 0
//	KBD_SCROLL_PORT_SEL1 &= ~KBD_SCROLL_PIN;			// i/o function selected, 1
	KBD_SCROLL_PIN_PORT_DIR &= ~KBD_SCROLL_PIN;		// input mode, 0
	KBD_SCROLL_PIN_PORT &= ~KBD_SCROLL_PIN_PORT;				// select pull-down mode, 0
	KBD_SCROLL_PIN_PULLDN |= KBD_SCROLL_PIN;			// enable internal pull up, 1


	KBD_PASSWORD_PIN_PORT_SEL0 &= ~KBD_PASSWORD_PIN_READ;					// i/o function selected, 0
//	KBD_INC_PORT_SEL1 &= ~KBD_INC_PIN;					// i/o function selected, 1
	KBD_PASSWORD_PIN_PORT_DIR &= ~KBD_PASSWORD_PIN_READ;				// input mode, 0
	KBD_PASSWORD_PIN_PORT &= ~KBD_PASSWORD_PIN_READ;						// select pull-down mode, 0
	KBD_PASSWORD_PIN_PULLDN |= KBD_PASSWORD_PIN_READ;					// enable internal pull up, 1
}

/*****************************************************************************************************
 Function   :  detectKey
 Description:  This Function used to detect the which  Key is pressed
 Parameters :  NONE
 Returns    :  detected key value
 Remarks    :
*******************************************************************************************************/
int detectKey()
{
    unsigned char breakFlag=0,passmatch=0;

    setupConter = 0;
	if (KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN)		////scroll key
	{
	    while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN)
        {
            if((gui_dispParameter==1) && (guc_SetupMode == 0))
            {
                setupConter++;
                //ms_delay(1);
                __delay_cycles(5000);
               // if(setupConter>1500)
                if(setupConter>750)
                {


                    if (KBD_PASSWORD_PIN_READ & KBD_PASSWORD_PIN)
                    {
                        setupConter=0;
                        guc_SetupMode=1;
                        display_ScaleFator(gul_VolperPulse);
                        breakFlag=0;
                        passmatch=0;
                        _no_operation();
                        break;
                    }
                    else if(setupConter>1500)
                    {
                        setupConter=0;

//                        gd_CT_LTR = 0;
//                        FRAM_gud_CT_LTR = gd_CT_LTR;
//                        display_Batch(gd_CT_LTR);
//                        _delay_cycles(10000);

                        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
                        FRAM_gud_CT_LTR = 0;
                        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
                        display_Batch(FRAM_gud_CT_LTR);
                        _delay_cycles(10000);

//                        while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
//                        breakFlag=1;
//                        passmatch=1;
//                        break;
                    }
                    while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                    breakFlag=1;
                    passmatch=1;
                    break;
                }
            }
            if((gui_dispParameter==1) && (guc_SetupMode == 1))
            {
                setupConter++;
                //ms_delay(1);
                __delay_cycles(5000);
               // if(setupConter>1500)
                if(setupConter>350)
                {
                    setupConter=0;
                    //while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                    breakFlag=1;
                    passmatch=1;
                    break;
                }
            }
            else if(gui_dispParameter==2)
            {
                setupConter++;
                //ms_delay(1);
                __delay_cycles(5000);
                if(setupConter>1500)
                {
                    setupConter=0;

                    if (KBD_PASSWORD_PIN_READ & KBD_PASSWORD_PIN)
                    {
//                        gdl_TT_LTR = 0;
//                        FRAM_gud_TT_LTR = gdl_TT_LTR;
//                        display_Total((unsigned long int)(gdl_TT_LTR));
//                        _delay_cycles(10000);
                        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
                        FRAM_gud_TT_LTR = 0;
                        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)
//                        while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                        breakFlag=1;
                        passmatch=1;
                        break;
                    }
                    while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
                    breakFlag=1;
                    passmatch=0;
                    break;
                }
            }
        }
        if ((breakFlag && passmatch) && (guc_SetupMode))
        {
//            while(KBD_SCROLL_PIN_READ & KBD_SCROLL_PIN);
            getKey = SAVE_DIGIT;
            return getKey ;
        }
        getKey = SCROLL_KEY;
        _no_operation();

	}
//	else if (KBD_PASSWORD_PIN_READ & KBD_PASSWORD_PIN)		//INC key
//	{
//        getKey = PASSWORD_KEY;
//        _no_operation();
//	}
	else
	{
		getKey = NO_KEY;			//none Key
	}

	if(lastgetKey != getKey)		//if Key is still pressed then do not update key variable
	{
		lastgetKey=getKey;
		return getKey;
	}
	else
	{
		return 0;
	}
}

