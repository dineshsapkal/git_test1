//#include <msp430.h>
//
//int main(void)
//{
//    WDTCTL = WDTPW | WDTHOLD;                     // Stop WDT
//
//    // Configure XT1 oscillator
//       P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins 323768KHz External Crystal
//       do
//       {
//           CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
//           SFRIFG1 &= ~OFIFG;
//       }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
//
//
//
//    // Configure GPIO
//    P1DIR |= BIT0;                                // P1.0 output
//    P1OUT |= BIT0;                                // P1.0 high
//
//    // Disable the GPIO power-on default high-impedance mode to activate
//    // previously configured port settings
//    PM5CTL0 &= ~LOCKLPM5;
//
//    TA0CCR0 = 164;
//    TA0CTL = TASSEL__ACLK | MC_0 | TACLR | TAIE; // SMCLK, count mode, clear TAR
//
//    __bis_SR_register(GIE);           // Enter LPM3 w/ interrupts
//    while(1);                          // For debugger
//}
//
//
//void __attribute__ ((interrupt(TIMER0_A1_VECTOR))) TIMER0_A1_ISR (void)
//{
//  switch(__even_in_range(TA0IV, TA0IV_TAIFG))
//  {
//    case TA0IV_NONE:   break;               // No interrupt
//    case TA0IV_TACCR1: break;               // CCR1 not used
//    case TA0IV_TACCR2: break;               // CCR2 not used
//    case TA0IV_3:      break;               // reserved
//    case TA0IV_4:      break;               // reserved
//    case TA0IV_5:      break;               // reserved
//    case TA0IV_6:      break;               // reserved
//    case TA0IV_TAIFG:                       // overflow
//                        P1OUT ^= BIT0;
//                        break;
//    default: break;
//  }
//}








#include <msp430.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;                    // Stop WDT



    // Configure GPIO
    P1DIR |= BIT0;
    P1OUT |= BIT0;
    // Configure XT1 oscillator
       P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins 323768KHz External Crystal
       do
       {
           CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
           SFRIFG1 &= ~OFIFG;
       }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag



    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

    // Configure Timer_A
    TA0CCR0 = 20;
    TA0CTL = TASSEL__ACLK | MC__UP |ID__8| TACLR | TAIE;      // ACLK, count mode, clear TAR, enable interrupt

    __bis_SR_register(LPM3_bits | GIE);           // Enter LPM3, enable interrupts
    __no_operation();                             // For debugger
}

// Timer0_A3 Interrupt Vector (TAIV) handler
void __attribute__ ((interrupt(TIMER0_A1_VECTOR))) TIMER0_A1_ISR (void)
{
    switch(__even_in_range(TA0IV,TA0IV_TAIFG))
    {
        case TA0IV_NONE:
            break;                               // No interrupt
        case TA0IV_TACCR1:
            break;                               // CCR1 not used
        case TA0IV_TACCR2:
            break;                               // CCR2 not used
        case TA0IV_3:
            break;                               // reserved
        case TA0IV_4:
            break;                               // reserved
        case TA0IV_5:
            break;                               // reserved
        case TA0IV_6:
            break;                               // reserved
        case TA0IV_TAIFG:
            P1OUT ^= BIT0;                       // overflow
            break;
        default:
            break;
    }
}





