
//******************************************************************************
#include <msp430.h>

 void display_digit(unsigned char,unsigned char);
 void display_Value(unsigned long);

 void display_Batch();
 void display_Total();
 void display_Flowrate();



int main( void )
{
    unsigned int len;
     WDTCTL = WDTPW | WDTHOLD;                                  // Stop watchdog timer

    // Configure XT1 oscillator
    P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
    CSCTL6 = (CSCTL6 & ~(XT1DRIVE_3)) | XT1DRIVE_2;            // Higher drive strength and current consumption for XT1 oscillator


    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

    // Configure LCD pins
    SYSCFG2 |=  LCDPCTL;                                        // R13/R23/R33/LCDCAP0/LCDCAP1 pins selected

    LCDPCTL0 = 0xFFC7;
    LCDPCTL1 = 0x3FFF;
    LCDPCTL2 = 0x0000;

    LCDCTL0 = LCDSSEL_0 | LCDDIV_7;                            // flcd ref freq is xtclk

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCDVCTL = LCDCPEN | LCDREFEN | VLCD_6 | (LCDCPFSEL0 | LCDCPFSEL1 | LCDCPFSEL2 | LCDCPFSEL3);

    LCDMEMCTL |= LCDCLRM;                                      // Clear LCD memory

    LCDCSSEL0 = 0x0007;                                        // Configure COMs and SEGs
    LCDCSSEL1 = 0x0000;                                        // L0, L1, L2COM pins
    LCDCSSEL2 = 0x0000;

    LCDM0 = 0x21;                                            // L0 = COM0, L1 = COM1
    LCDM1 = 0x04;                                            // L2 = COM2,



    LCDCTL0 |= LCD3MUX | LCDON;                                // Turn on LCD, 3-mux selected

    __no_operation();                                          // For debugger.




    while(1)
    {

        for(len=0;len<=99999999;len++)
        {
//            display_digit(len,8);
//            display_digit(len,7);
//            display_digit(len,6);
//            display_digit(len,5);
//            display_digit(len,4);
//            display_digit(len,3);
//            display_digit(len,2);
//            display_digit(len,1);
//            display_Value(len);
//            _delay_cycles(100000);
            display_Batch();
            _delay_cycles(500000);
            display_Total();
            _delay_cycles(500000);
            display_Flowrate();
            _delay_cycles(500000);
        }
    }

}


void display_digit(unsigned char dispNum,unsigned char pos)
{
   if(pos==8)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[3] = 0x53;
                   LCDMEM[4] = 0x03;
                   break;
           case 1:
                   LCDMEM[3] = 0x03;
                   LCDMEM[4] = 0x00;
                   break;
           case 2:
                   LCDMEM[3] = 0x71;
                   LCDMEM[4] = 0x02;
                   break;
           case 3:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = 0x00;
                   break;
           case 4:
                   LCDMEM[3] = 0x23;
                   LCDMEM[4] = 0x01;
                   break;
           case 5:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = 0x01;
                   break;
           case 6:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = 0x03;
                   break;
           case 7:
                   LCDMEM[3] = 0x13;
                   LCDMEM[4] = 0x00;
                   break;
           case 8:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = 0x03;
                   break;
           case 9:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = 0x01;
                   break;
       }
   }
   else if(pos==7)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x35;
                   break;
           case 1:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x00;
                   break;
           case 2:
                   LCDMEM[4] = LCDMEM[4] | 0x10;
                   LCDMEM[5] = 0x27;
                   break;
           case 3:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x07;
                   break;
           case 4:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x12;
                   break;
           case 5:
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x17;
                   break;
           case 6:
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x37;
                   break;
           case 7:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x01;
                   break;
           case 8:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x37;
                   break;
           case 9:
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x17;
                   break;
       }
   }
   else  if(pos==6)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[6] = 0x53;
                   LCDMEM[7] = 0x03;
                   break;
           case 1:
                   LCDMEM[6] = 0x03;
                   LCDMEM[7] = 0x00;
                   break;
           case 2:
                   LCDMEM[6] = 0x71;
                   LCDMEM[7] = 0x02;
                   break;
           case 3:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = 0x00;
                   break;
           case 4:
                   LCDMEM[6] = 0x23;
                   LCDMEM[7] = 0x01;
                   break;
           case 5:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = 0x01;
                   break;
           case 6:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = 0x03;
                   break;
           case 7:
                   LCDMEM[6] = 0x13;
                   LCDMEM[7] = 0x00;
                   break;
           case 8:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = 0x03;
                   break;
           case 9:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = 0x01;
                   break;
       }
   }
   else  if(pos==5)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x35;
                   break;
           case 1:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x00;
                   break;
           case 2:
                   LCDMEM[7] = LCDMEM[7] | 0x10;
                   LCDMEM[8] = LCDMEM[8] | 0x27;
                   break;
           case 3:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x07;
                   break;
           case 4:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x12;
                   break;
           case 5:
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
           case 6:
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 7:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x01;
                   break;
           case 8:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 9:
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
       }
   }
   else  if(pos==4)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[9] = 0x53;
                   LCDMEM[10] = 0x03;
                   break;
           case 1:
                   LCDMEM[9] = 0x03;
                   LCDMEM[10] = 0x00;
                   break;
           case 2:
                   LCDMEM[9] = 0x71;
                   LCDMEM[10] = 0x02;
                   break;
           case 3:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = 0x00;
                   break;
           case 4:
                   LCDMEM[9] = 0x23;
                   LCDMEM[10] = 0x01;
                   break;
           case 5:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = 0x01;
                   break;
           case 6:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = 0x03;
                   break;
           case 7:
                   LCDMEM[9] = 0x13;
                   LCDMEM[10] = 0x00;
                   break;
           case 8:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = 0x03;
                   break;
           case 9:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = 0x01;
                   break;
       }
   }
   else  if(pos==3)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x35;
                   break;
           case 1:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x00;
                   break;
           case 2:
               _nop();
                   LCDMEM[10] = LCDMEM[10] | 0x10;
                   LCDMEM[11] = LCDMEM[11] | 0x27;
                   break;
           case 3:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x07;
                   break;
           case 4:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x12;
                   break;
           case 5:
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
           case 6:
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 7:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x01;
                   break;
           case 8:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 9:
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
       }
   }
   else  if(pos==2)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[12] = 0x53;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 1:
                   LCDMEM[12] = 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 2:
                   LCDMEM[12] = 0x71;
                   LCDMEM[13] = LCDMEM[13] | 0x02;
                   break;
           case 3:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 4:
                   LCDMEM[12] = 0x23;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 5:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 6:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 7:
                   LCDMEM[12] = 0x13;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 8:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 9:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
       }
   }
   else  if(pos==1)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x35;
                   break;
           case 1:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x00;
                   break;
           case 2:
                   LCDMEM[13] = LCDMEM[13] | 0x10;
                   LCDMEM[14] = LCDMEM[14] | 0x27;
                   break;
           case 3:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x07;
                   break;
           case 4:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x12;
                   break;
           case 5:
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
           case 6:
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 7:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x01;
                   break;
           case 8:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 9:
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
       }
   }

}



void display_Value(unsigned long Value)
{
    if(Value<=9)
    {
        display_digit(((Value%10)),8);
    }
    else if(Value>9 && Value<=99)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
    }
    else if(Value>99 && Value<=999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
    }
    else if(Value>999 && Value<=9999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
        display_digit(((Value/1000)%10),5);
    }
    else if(Value>9999 && Value<=99999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
        display_digit(((Value/1000)%10),5);
    }
    else if(Value>99999 && Value<=999999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
        display_digit(((Value/1000)%10),5);
        display_digit(((Value/10000)%10),4);
        display_digit(((Value/100000)%10),3);
    }
    else if(Value>999999 && Value<=9999999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
        display_digit(((Value/1000)%10),5);
        display_digit(((Value/10000)%10),4);
        display_digit(((Value/100000)%10),3);
        display_digit(((Value/1000000)%10),2);
    }
    else if(Value>9999999 && Value<=99999999)
    {
        display_digit(((Value%10)),8);
        display_digit(((Value/10)%10),7);
        display_digit(((Value/100)%10),6);
        display_digit(((Value/1000)%10),5);
        display_digit(((Value/10000)%10),4);
        display_digit(((Value/100000)%10),3);
        display_digit(((Value/1000000)%10),2);
        display_digit((Value/10000000),1);
    }
}


void display_Batch()
{
    //for display Batch LTR
    LCDMEM[14] = 0x40;
    LCDMEM[8] = 0x40;
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;

    display_Value(1234);
}
void display_Total()
{
    //for display Total LTS
    LCDMEM[13] = 0xFF;
    LCDMEM[8] = 0xFF;
    LCDMEM[14] = 0xFF;

    display_Value(5678);
}
void display_Flowrate()
{

    //for display Flow Rate
    LCDMEM[11] = 0x40;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(95686);
}




