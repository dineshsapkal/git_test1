/*
 * basic_requirement.h
 *
 *  Created on: 11-Jan-2018
 *      Author: Dell
 */

#ifndef BASIC_REQUIREMENT_H_
#define BASIC_REQUIREMENT_H_



#include <msp430.h>

#include "setup.h"
#include "segment_lcd.h"



extern void initiate_Oscillator();
extern void init_gpio();


#endif /* BASIC_REQUIREMENT_H_ */
