/*
 * segment_lcd.c
 *
 *  Created on: 11-Jan-2018
 *      Author: Dell
 */




#include "basic_requirement.h"

unsigned char d1,d2,d3,d4,d5,d6,d7,d8;

void clear_data()
{
    LCDMEM[3] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[4] = 0x00;
    LCDMEM[5] = 0x00;
    LCDMEM[6] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[7] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[9] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[10] = 0x00;
    LCDMEM[11] = 0x00;
    LCDMEM[12] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[14] = 0x00;
}

void display_Batch()
{
    d8 =  (long)((FRAM_gud_CT_LTR*10))%10;

    d7 = ((long)FRAM_gud_CT_LTR%10);
    d6 = ((long)FRAM_gud_CT_LTR/10)%10;
    d5 = ((long)FRAM_gud_CT_LTR/100)%10;
    d4 = ((long)FRAM_gud_CT_LTR/1000)%10;
    d3 = ((long)FRAM_gud_CT_LTR/10000)%10;
    d2 = ((long)FRAM_gud_CT_LTR/100000)%10;
    d1 = ((long)FRAM_gud_CT_LTR/1000000);


    clear_data();

    //for display Batch LTR
    LCDMEM[14] = 0x40;
    LCDMEM[8] = 0x40;
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;

    if((long)FRAM_gud_CT_LTR<=9)
    {
       display_digit(d8,8);
       display_digit(d7,7);
    }
    else if((long)FRAM_gud_CT_LTR>9 && (long)FRAM_gud_CT_LTR<=99)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if((long)FRAM_gud_CT_LTR>99 && (long)FRAM_gud_CT_LTR<=999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);

    }
    else if((long)FRAM_gud_CT_LTR>999 && (long)FRAM_gud_CT_LTR<=9999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if((long)FRAM_gud_CT_LTR>9999 && (long)FRAM_gud_CT_LTR<=99999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if((long)FRAM_gud_CT_LTR>99999 && (long)FRAM_gud_CT_LTR<=999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if((long)FRAM_gud_CT_LTR>999999 && (long)FRAM_gud_CT_LTR<=9999999)
    {
        display_digit(d8,8);

        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}

void display_Total(unsigned long int Total)
{
    d8 =  Total%10;
    d7 = (Total/10)%10;
    d6 = (Total/100)%10;
    d5 = (Total/1000)%10;
    d4 = (Total/10000)%10;
    d3 = (Total/100000)%10;
    d2 = (Total/1000000)%10;
    d1 = (Total/10000000);

    clear_data();
    //for display Total LTS
    LCDMEM[13] = 0x04;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Total);
}
void display_Flowrate(unsigned long int Flow)
{
    d8 =  Flow%10;
    d7 = (Flow/10)%10;
    d6 = (Flow/100)%10;
    d5 = (Flow/1000)%10;
    d4 = (Flow/10000)%10;
    d3 = (Flow/100000)%10;
    d2 = (Flow/1000000)%10;
    d1 = (Flow/10000000);

    if(Flow<=99999999)
    {
        clear_data();
    }

    //for display Flow Rate
    LCDMEM[11] = 0x40;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    display_Value(Flow);
}


void display_Value(unsigned long Value)
{
    if(Value<=9)
    {
        display_digit(d8,8);
    }
    else if(Value>9 && Value<=99)
    {
        display_digit(d8,8);
        display_digit(d7,7);
    }
    else if(Value>99 && Value<=999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
    }
    else if(Value>999 && Value<=9999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
    }
    else if(Value>9999 && Value<=99999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
    }
    else if(Value>99999 && Value<=999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
    }
    else if(Value>999999 && Value<=9999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
    }
    else if(Value>9999999 && Value<=99999999)
    {
        display_digit(d8,8);
        display_digit(d7,7);
        display_digit(d6,6);
        display_digit(d5,5);
        display_digit(d4,4);
        display_digit(d3,3);
        display_digit(d2,2);
        display_digit(d1,1);
    }
}

void display_ScaleFator(unsigned long int ScaleFactor)
{

    clear_data();

    //for display Flow Rate
    LCDMEM[11] = 0x00;
    LCDMEM[13] = 0x00;
    LCDMEM[8] = 0x00;
    LCDMEM[14] = 0x00;

    displayScale(ScaleFactor);
}


void displayScale(unsigned long Value)
{
    display_digit(((Value%10)),8);
    display_digit(((Value/10)%10),7);
    display_digit(((Value/100)%10),6);
    display_digit(((Value/1000)%10),5);
    display_digit(((Value/10000)%10),4);
    display_digit(((Value/100000)%10),3);
    display_digit(((Value/1000000)%10),2);
    display_digit((Value/10000000),1);
}
