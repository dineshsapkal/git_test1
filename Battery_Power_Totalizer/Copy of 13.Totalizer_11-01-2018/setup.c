/*
 * setup.c
 *
 *  Created on: 25-Dec-2017
 *      Author: Dell
 */


#include "basic_requirement.h"

#pragma PERSISTENT(FRAM_guiVolperPulse)
unsigned long FRAM_guiVolperPulse = 0;


unsigned char guc_SetupMode,guc_saveDataFlag;

float gf_VolperPulse;
//unsigned long gul_VolperPulse;

unsigned char gucReset_CT_Flag=0,gucReset_TT_Flag=0;


void wait()
{
    unsigned long gulCounter=25000;
    __no_operation();
    do
    {
        gulCounter--;
        __no_operation();
    }while(gulCounter>1);
}


void setup_Mode()
{
    unsigned char digit8=0,digit7=0,digit6=0,digit5=0,digit4=0,digit3=0,digit2=0,digit1=0;
    char breakLoopFlag=0;


    if(guc_SetupMode)
    {
        display_ScaleFator(FRAM_guiVolperPulse);

        digit8 = (FRAM_guiVolperPulse%10);
        digit7 = (FRAM_guiVolperPulse/10)%10;
        digit6 = (FRAM_guiVolperPulse/100)%10;
        digit5 = (FRAM_guiVolperPulse/1000)%10;
        digit4 = (FRAM_guiVolperPulse/10000)%10;
        digit3 = (FRAM_guiVolperPulse/100000)%10;
        digit2 = (FRAM_guiVolperPulse/1000000)%10;
        digit1 = (FRAM_guiVolperPulse/10000000);

        _nop();
        //Digit8
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit8++;
                if(digit8>=10)
                {
                    digit8 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,8);
            wait();

            display_digit(digit8,8);
            wait();


            if(breakLoopFlag)
            {
                display_digit(digit8,8);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit7
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit7++;
                if(digit7>=10)
                {
                    digit7 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,7);
            wait();

            display_digit(digit7,7);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit7,7);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit6
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit6++;
                if(digit6>=10)
                {
                    digit6 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,6);
            wait();

            display_digit(digit6,6);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit6,6);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit5
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit5++;
                if(digit5>=10)
                {
                    digit5 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,5);
            wait();

            display_digit(digit5,5);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit5,5);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit4
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit4++;
                if(digit4>=10)
                {
                    digit4 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,4);
            wait();

            display_digit(digit4,4);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit4,4);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit3
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit3++;
                if(digit3>=10)
                {
                    digit3 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,3);
            wait();

            display_digit(digit3,3);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit3,3);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit2
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit2++;
                if(digit2>=10)
                {
                    digit2 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,2);
            wait();

            display_digit(digit2,2);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit2,2);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();
        //Digit1
        while(1)
        {
            if((gucButtonPressFlag) && (guc_saveDataFlag ==0))
            {
                gucButtonPressFlag=0;
                digit1++;
                if(digit1>=10)
                {
                    digit1 = 0;
                }
            }
            else if((gucButtonPressFlag) && (guc_saveDataFlag))
            {
                gucButtonPressFlag=0;
                guc_saveDataFlag=0;
                breakLoopFlag=1;
            }

            _nop();

            display_digit(10,1);
            wait();

            display_digit(digit1,1);
            wait();

            if(breakLoopFlag)
            {
                display_digit(digit1,1);
                breakLoopFlag=0;
                break;
            }
        }
        _nop();

        SYSCFG0 &= ~PFWP;                   // Program FRAM write enable
        FRAM_guiVolperPulse = ((unsigned long)digit1 * 10000000) + ((unsigned long)digit2 * 1000000) + ((unsigned long)digit3 * 100000) + ((unsigned long)digit4 * 10000) + (digit5 * 1000) + (digit6 * 100) + (digit7 * 10) + (digit8 * 1);
        SYSCFG0 |= PFWP;                    // Program FRAM write protected (not writable)

        gf_VolperPulse = (float)FRAM_guiVolperPulse/10000;
        gf_VolperPulse = gf_VolperPulse/1000;
        _nop();



        _no_operation();
        guc_SetupMode = 0;
    }
}




void display_digit(unsigned char dispNum,unsigned char pos)
{
   if(pos==8)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[3] = 0x53;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 1:
                   LCDMEM[3] = 0x03;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 2:
                   LCDMEM[3] = 0x71;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x02;
                   break;
           case 3:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 4:
                   LCDMEM[3] = 0x23;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 5:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 6:
                   LCDMEM[3] = 0x72;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 7:
                   LCDMEM[3] = 0x13;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   break;
           case 8:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x03;
                   break;
           case 9:
                   LCDMEM[3] = 0x73;
                   LCDMEM[4] = LCDMEM[4] & 0x30;
                   LCDMEM[4] = LCDMEM[4] | 0x01;
                   break;
           case 10:
                  LCDMEM[3] = 0x00;
                  LCDMEM[4] = LCDMEM[4] & 0x30;
                  LCDMEM[4] = LCDMEM[4] | 0x00;
                  break;
       }
   }
   else if(pos==7)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x35;
                   break;
           case 1:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x00;
                   break;
           case 2:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x10;
                   LCDMEM[5] = 0x27;
                   break;
           case 3:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x07;
                   break;
           case 4:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x12;
                   break;
           case 5:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x17;
                   break;
           case 6:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x20;
                   LCDMEM[5] = 0x37;
                   break;
           case 7:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x01;
                   break;
           case 8:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x37;
                   break;
           case 9:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x30;
                   LCDMEM[5] = 0x17;
                   break;
           case 10:
                   LCDMEM[4] = LCDMEM[4] & 0x03;
                   LCDMEM[4] = LCDMEM[4] | 0x00;
                   LCDMEM[5] = 0x00;
                   break;
       }
   }
   else  if(pos==6)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[6] = 0x53;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 1:
                   LCDMEM[6] = 0x03;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 2:
                   LCDMEM[6] = 0x71;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x02;
                   break;
           case 3:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 4:
                   LCDMEM[6] = 0x23;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 5:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 6:
                   LCDMEM[6] = 0x72;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 7:
                   LCDMEM[6] = 0x13;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
           case 8:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x03;
                   break;
           case 9:
                   LCDMEM[6] = 0x73;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x01;
                   break;
           case 10:
                   LCDMEM[6] = 0x00;
                   LCDMEM[7] = LCDMEM[7] & 0x30;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   break;
       }
   }
   else  if(pos==5)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x35;
                   break;
           case 1:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x00;
                   break;
           case 2:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x10;
                   LCDMEM[8] = LCDMEM[8] | 0x27;
                   break;
           case 3:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x07;
                   break;
           case 4:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x12;
                   break;
           case 5:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
           case 6:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x20;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 7:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x01;
                   break;
           case 8:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x37;
                   break;
           case 9:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x30;
                   LCDMEM[8] = LCDMEM[8] | 0x17;
                   break;
           case 10:
                   LCDMEM[7] = LCDMEM[7] & 0x03;
                   LCDMEM[7] = LCDMEM[7] | 0x00;
                   LCDMEM[8] = 0x00;
                   break;
       }
   }
   else  if(pos==4)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[9] = 0x53;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 1:
                   LCDMEM[9] = 0x03;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 2:
                   LCDMEM[9] = 0x71;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x02;
                   break;
           case 3:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 4:
                   LCDMEM[9] = 0x23;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 5:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 6:
                   LCDMEM[9] = 0x72;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 7:
                   LCDMEM[9] = 0x13;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
           case 8:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x03;
                   break;
           case 9:
                   LCDMEM[9] = 0x73;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x01;
                   break;
           case 10:
                   LCDMEM[9] = 0x00;
                   LCDMEM[10] = LCDMEM[10] & 0x30;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   break;
       }
   }
   else  if(pos==3)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x35;
                   break;
           case 1:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x00;
                   break;
           case 2:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x10;
                   LCDMEM[11] = LCDMEM[11] | 0x27;
                   break;
           case 3:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x07;
                   break;
           case 4:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x12;
                   break;
           case 5:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
           case 6:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x20;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 7:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x01;
                   break;
           case 8:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x37;
                   break;
           case 9:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x30;
                   LCDMEM[11] = LCDMEM[11] | 0x17;
                   break;
           case 10:
                   LCDMEM[10] = LCDMEM[10] & 0x03;
                   LCDMEM[10] = LCDMEM[10] | 0x00;
                   LCDMEM[11] = 0x00;
                   break;
       }
   }
   else  if(pos==2)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[12] = 0x53;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 1:
                   LCDMEM[12] = 0x03;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 2:
                   LCDMEM[12] = 0x71;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x02;
                   break;
           case 3:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 4:
                   LCDMEM[12] = 0x23;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 5:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 6:
                   LCDMEM[12] = 0x72;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 7:
                   LCDMEM[12] = 0x13;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
           case 8:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x03;
                   break;
           case 9:
                   LCDMEM[12] = 0x73;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x01;
                   break;
           case 10:
                   LCDMEM[12] = 0x00;
                   LCDMEM[13] = LCDMEM[13] & 0x30;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   break;
       }
   }
   else  if(pos==1)
   {
       switch(dispNum)
       {
           case 0:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x35;
                   break;
           case 1:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x00;
                   break;
           case 2:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x10;
                   LCDMEM[14] = LCDMEM[14] | 0x27;
                   break;
           case 3:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x07;
                   break;
           case 4:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x12;
                   break;
           case 5:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
           case 6:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x20;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 7:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x01;
                   break;
           case 8:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x37;
                   break;
           case 9:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x30;
                   LCDMEM[14] = LCDMEM[14] | 0x17;
                   break;
           case 10:
                   LCDMEM[13] = LCDMEM[13] & 0x03;
                   LCDMEM[13] = LCDMEM[13] | 0x00;
                   LCDMEM[14] = 0x00;
                   break;
       }
   }

}
