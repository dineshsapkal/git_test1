/*
 * basic_requirement.c
 *
 *  Created on: 11-Jan-2018
 *      Author: Dell
 */


#include "basic_requirement.h"

void initiate_Oscillator()
{
    // Configure XT1 oscillator
    P4SEL0 |= BIT1 | BIT2;                                     // P4.2~P4.1: crystal pins 323768KHz External Crystal
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);                         // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    }while (SFRIFG1 & OFIFG);                                  // Test oscillator fault flag
}


void init_gpio()
{
    // Configure all GPIO to Output Low
    P1OUT = 0x20;P2OUT = 0x00;P3OUT = 0x00;P4OUT = 0x00;
    P5OUT = 0x02;P6OUT = 0x00;P7OUT = 0x00;P8OUT = 0x00;

    P1DIR = 0x5F;P2DIR = 0xFF;P3DIR = 0xFF;P4DIR = 0xFF;
    P5DIR = 0xFD;P6DIR = 0xFF;P7DIR = 0xFF;P8DIR = 0xFF;
}
